#  Copyright (c) 2023. Gabriele Proietti Mattia <pm.gabriele@gmail.com>
#  simulator-2023-cloudcom - Simpy simulator of energy schedulers
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.

from __future__ import annotations

import random
from enum import Enum
from typing import List

import simpy

from log import Log


class Job:
    _MODULE = "Job"

    class TransmissionAction(Enum):
        """Client <--> Node A <--> Node B"""
        CLIENT_TO_NODE_A = 0
        NODE_A_TO_NODE_B = 1
        NODE_B_TO_NODE_A = 2
        NODE_A_TO_CLIENT = 3

    class DurationType(Enum):
        FIXED = 0
        GAUSSIAN = 1
        EXPONENTIAL = 2

    class RejectionCause(Enum):
        NODE_NOT_ALIVE = 0
        NODE_SATURATED = 1

    def __init__(self, env: simpy.Environment,
                 uid=0,
                 node_uid=0,
                 job_type=0,
                 end_clb=None,
                 duration=1.0,
                 duration_std_dev=0.0013,
                 eps=0.0,
                 payload_size_mbytes=0.1,
                 episode=0):
        """Create a new job"""
        self._uid = f"{node_uid}-{uid}"
        """Job identifier"""
        self._node_uid = node_uid
        """Job node generator identifier"""
        self._env = env
        """Simpy environment"""
        self._job_duration = duration  # s
        """Average duration of a job"""
        self._job_duration_std_dev = duration_std_dev  # s
        """Standard deviation of a job"""
        self._payload_size = payload_size_mbytes
        """Size of the payload in mbytes"""

        self._transmission_next_action = Job.TransmissionAction.CLIENT_TO_NODE_A
        self._transmission_actions_list = [Job.TransmissionAction.CLIENT_TO_NODE_A]
        self._transmission_actions_list_timings = [0.0]

        # realtime check
        self._job_type = job_type

        # metrics
        self._metric_execution_time = 0.0  # s
        self._metric_queue_time = 0.0  # s

        # time snapshots
        self._time_generated = env.now
        self._time_queued = 0.0
        self._time_executed = 0.0
        self._time_done = 0.0
        self._time_lost = 0.0
        self._time_forwarded = 0.0
        self._time_probing = 0.0
        self._time_rejected = 0.0
        self._time_dispatched = 0.0
        """Time at which"""

        self._forwarded_to_node_id = -1
        self._forwarded_to_cluster_id = -1
        self._forwarded_to_cloud = False

        self._forwarded_to_node_index = -1
        """Forwarded to node neighbour index from originator node"""

        # self._current_probing_time = 0.0  # s
        self._dispatched = False
        self._forwarded = False
        self._rejected = False
        self._executed = False
        self._lost = False
        self._done = False
        """If job path has ended"""

        self._rejection_cause = None
        """The cause for rejection"""

        # data to be logged
        # callbacks
        self._end_clb = end_clb
        self._episode = episode
        """Episode to which the job belongs"""
        self._last = False
        """If it is the last job of the episode"""
        self._eps = eps
        """The eps value for the job"""

        # learning
        self._learning_state = []
        self._learning_action = 0
        self._learning_reward = 0.0

        self._state_updated = []

        self._lifespan_originator = 0.0
        self._lifespan_destination = 0.0

    def __str__(self):
        return f"Job#{self._uid}#N{self._node_uid}#E{self._episode}"

    #
    # Actions
    #

    def a_dispatched(self):
        self._time_dispatched = self._env.now
        self._dispatched = True

    def a_in_queue(self):
        """Add time in queue"""
        self._time_queued = self._env.now

    def a_executed(self, job_duration: float):
        self._metric_execution_time = job_duration
        self._executed = True

        self._time_executed = self._env.now

    def a_forwarded(self, to_node_uid=-1, to_node_index=-1, to_cloud=False):
        """Execute when a job is forwarded to another node"""
        if to_node_uid == -1 and to_cloud is False:
            Log.mfatal(self._module(), "You called the forwarded action with no parameters")
        if to_node_uid == self._node_uid:
            # raise RuntimeError(f"You forwarded the job to the same node: "
            #                    f"originator={self._node_uid} "
            #                    f"to_node_id={to_node_uid} "
            #                    f"to_node_index={to_node_index}")
            Log.mfatal(self._module(), f"You forwarded the job to the same node: "
                                       f"originator={self._node_uid} "
                                       f"to_node_id={to_node_uid} "
                                       f"to_node_index={to_node_index}")

        self._forwarded = True
        self._time_forwarded = self._env.now
        self._forwarded_to_node_id = to_node_uid
        self._forwarded_to_node_index = to_node_index
        self._forwarded_to_cloud = to_cloud

    def a_rejected(self, rejection_cause: Job.RejectionCause):
        self._rejected = True
        self._time_rejected = self._env.now

    def a_lost(self):
        self._lost = True
        self._time_lost = self._env.now

    def a_done(self):
        """Call this function when job path end, i.e. job output reached the client"""
        # safety checks
        if not self._executed and not self._rejected and not self._lost:
            Log.mfatal(self._module(),
                       f"You declared done {self} without being executed or rejected, dispatched={self._dispatched} forwarded={self._forwarded}")
        if (self._rejected or (self._executed and self._forwarded)) \
                and not (len(self._transmission_actions_list) in [2, 4]) and not self._lost:
            Log.mfatal(self._module(), f"Job rejected with bad action list: {self}, "
                                       f"executed={self._executed}, "
                                       f"rejected={self._rejected}, "
                                       f"forward={self._forwarded}, "
                                       f"originator={self.get_originator_node_uid()}, "
                                       f"forwarded_to={self._forwarded_to_node_id}, "
                                       f"{self._transmission_actions_list}")

        self._done = True
        self._time_done = self._env.now

        if self._end_clb is not None:
            self._end_clb(self)

        # if self.get_total_time() > 0.1:
        #     print(self._transmission_actions_list)
        #     print(self._transmission_actions_list_timings)
        #     raise RuntimeError(f"TooMuch: {self.get_total_time()}")

    def _module(self):
        return f"{Job._MODULE}#N{self.get_node_uid()}#ID{self.get_uid()}"

    #
    # Exported
    #

    @staticmethod
    def compute_duration(job, job_duration_type, machine_speed: float):
        # compute the job duration given the machine speed
        job_duration = job.get_job_duration() * (1 / machine_speed)

        # choose the duration distribution
        if job_duration_type == Job.DurationType.GAUSSIAN:
            actual_job_duration = -1.0
            # loop until we get non-negative durations
            while actual_job_duration <= 0.0:
                actual_job_duration = random.gauss(job_duration, pow(job.get_job_duration_std_dev(), 2))
            return actual_job_duration
        elif job_duration_type == Job.DurationType.EXPONENTIAL:
            return random.expovariate(1 / job_duration)
        else:
            return job_duration

    def get_generated_at(self):
        return self._time_generated

    def get_transmission_next_action(self) -> TransmissionAction:
        return self._transmission_next_action

    def get_transmission_actions_list(self) -> List[TransmissionAction]:
        return self._transmission_actions_list

    def is_rejected(self):
        return self._rejected

    def is_forwarded(self):
        return self._forwarded

    def is_lost(self):
        return self._lost

    def is_forwarded_to_cloud(self):
        return self._forwarded_to_cloud

    def get_forwarded_to_cluster_id(self):
        return self._forwarded_to_cluster_id

    # def is_dispatched(self):
    #     return self._dispatched

    def is_succeed(self):
        return self.is_executed() and not self.is_rejected()

    def get_type(self) -> int:
        return self._job_type

    def is_executed(self):
        """Get if job has been executed. Obviously, job rejected are not executed"""
        return self._executed

    def is_done(self):
        """Get if job has been executed. Obviously, job rejected are not executed"""
        return self._done

    def get_total_time(self) -> float:
        """Get the total elapsed time for executing the job"""
        # if not self._done:
        #    Log.mwarn(self.MODULE, "Requesting total time while job has not done")
        return self._time_done - self._time_generated

    def get_originator_node_uid(self):
        return self._node_uid

    def get_forwarded_to_node_id(self):
        return self._forwarded_to_node_id

    def get_forwarded_to_node_index(self):
        return self._forwarded_to_node_index

    def get_probing_time(self):
        return self._time_probing - self._time_generated

    def get_dispatched_time(self):
        return self._time_dispatched - self._time_generated

    def get_queue_time(self):
        return self._time_queued - self._time_generated

    def get_duration(self):
        """Get the programmed job duration"""
        return self._job_duration

    def get_payload_size(self):
        return self._payload_size

    def get_episode(self):
        return self._episode

    def get_eps(self):
        return self._eps

    def get_uid(self):
        return self._uid

    def get_node_uid(self):
        return self._node_uid

    def get_learning_reward(self):
        return self._learning_reward

    def get_learning_state(self):
        return self._learning_state

    def get_learning_action(self):
        return self._learning_action

    def is_last_of_episode(self):
        return self._last

    def set_transmission_next_action(self, action: Job.TransmissionAction):
        if action == self._transmission_actions_list[-1]:
            raise RuntimeError(
                f"Job#{self.get_uid()}: You are setting the same action ({action.name}) two times: list={self._transmission_actions_list}")

        self._transmission_next_action = action
        self._transmission_actions_list.append(action)
        self._transmission_actions_list_timings.append(self._env.now - self._time_generated)

    def set_episode(self, episode):
        self._episode = episode

    def set_last_of_episode(self, last):
        self._last = last

    def set_eps(self, eps):
        self._eps = eps

    def set_state_updated(self, action, leaving):
        self._state_updated.append(f"{action}-{leaving}")

    def set_learning_reward(self, r):
        self._learning_reward = r

    def set_learning_state(self, state):
        self._learning_state = state

    def set_learning_action(self, action):
        self._learning_action = action

    def set_lifespan_originator(self, ls):
        self._lifespan_originator = ls

    def set_lifespan_destination(self, ls):
        self._lifespan_destination = ls

    #
    # Time generators
    #

    def get_job_duration(self) -> float:
        return self._job_duration

    def get_job_duration_std_dev(self) -> float:
        return self._job_duration_std_dev

    def get_total_time_execution(self) -> float:
        return self._metric_execution_time

    def get_time_execution(self):
        return self._time_executed - self._time_generated

    def get_lifespan_originator(self):
        return self._lifespan_originator

    def get_lifespan_destination(self):
        return self._lifespan_destination
