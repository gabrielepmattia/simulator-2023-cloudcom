#  Copyright (c) 2023. Gabriele Proietti Mattia <pm.gabriele@gmail.com>
#  simulator-2023-cloudcom - Simpy simulator of energy schedulers
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.

from __future__ import annotations

import random
from enum import Enum
from typing import List

import simpy

from job import Job
from log import Log
from model_solar import SolarPowerModel
from model_traffic import TrafficModel
from models import SolarTraceSpec
from reinforcement import TrainingSample

"""
Implementation of a node in a fog environment
"""


# noinspection DuplicatedCode
class Node:
    _MODULE = "Node"
    _DEBUG = False

    class DistributionArrivals(Enum):
        POISSON = 0
        DETERMINISTIC = 1
        GAUSSIAN = 2

    class DistributionNetworkLatency(Enum):
        DETERMINISTIC = 0
        GAUSSIAN = 1

    class SchedulingAction(Enum):
        EXECUTE_LOCALLY = 0
        FORWARD_TO_NEIGHBOUR = 1

    class SchedulerType(Enum):
        REINFORCEMENT_LEARNING = 0
        FIXED_POLICY = 1

    class SchedulerFixedPolicyAlgorithm(Enum):
        NO_SCHEDULER = 0
        RATIO_ADAPTIVE_STEP = 1

    # noinspection PyUnresolvedReferences,PyDefaultArgument
    def __init__(self,
                 env=None,
                 node_uid=None,
                 simulation_time=50000,
                 discovery=None,
                 data_storage=None,
                 round_time_s=30,
                 episode_n=0,
                 stop_when_node_shutdown=False,
                 # scheduler
                 scheduler_type=SchedulerType.FIXED_POLICY,
                 scheduler_fixed_policy_algorithm=SchedulerFixedPolicyAlgorithm.RATIO_ADAPTIVE_STEP,
                 # machine
                 max_jobs_in_queue=10,
                 # network
                 net_speed_client_node_mbits=200,
                 net_speed_node_node_mbits=300,
                 # traffic
                 rate_l=1.0,
                 rate_l_model_path=None,
                 rate_l_model_path_cycles=1,
                 rate_l_model_path_shift=0,
                 rate_l_model_path_parse_x_max=None,
                 rate_l_model_path_steady=False,
                 rate_l_model_path_steady_for=2000,
                 rate_l_model_path_steady_every=2000,
                 rate_mu=1.0,
                 job_payload_size_mb=0.100,
                 # distributions
                 distribution_arrivals=DistributionArrivals.POISSON,
                 distribution_network_latency=DistributionNetworkLatency.GAUSSIAN,
                 distribution_network_forwarding_sigma=0.00001,
                 # energy
                 battery_total_capacity_wh=10,
                 battery_initial_capacity_wh=10,
                 energy_cpu_w=2.97,
                 energy_transmission_w=0.015,
                 energy_idle_w=2.5,
                 # solar
                 solar_trace_enabled=False,
                 solar_trace_csv="",
                 solar_trace_spec=None,
                 solar_trace_csv_col=1,
                 solar_trace_to_watt_multiplier=1000,
                 solar_trace_efficiency=1.0,
                 # learning
                 eps_initial=0.99,
                 eps_decay=0.9999,
                 eps_min=0.05,
                 reinforcement_learning_algorithm=None,
                 train_every_samples_n=150,
                 ):

        #
        # Fixed variables
        #

        """Total time of the simulation"""
        self._uid = random.randint(0, 100000) if node_uid is None else node_uid
        self._env = env  # type: 'simpy.Environment'

        # node parameters
        self._simulation_time = simulation_time
        """Maximum simulation time"""

        # machine
        self._max_jobs_in_queue = max_jobs_in_queue  # = to 'k'
        """Maximum number of jobs in the queue"""

        self._stop_when_node_shutdown = stop_when_node_shutdown
        """If node should stop when it reaches zero battery"""

        # scheduler
        self._scheduler_type = scheduler_type
        self._scheduler_fixed_policy_algorithm = scheduler_fixed_policy_algorithm

        # energy
        self._battery_total_capacity_wh = battery_total_capacity_wh
        self._battery_initial_capacity_wh = battery_initial_capacity_wh
        self._battery_current_capacity_wh = battery_initial_capacity_wh
        self._battery_last_round_capacity_wh = battery_initial_capacity_wh

        self._energy_cpu_w = energy_cpu_w
        self._energy_transmission_w = energy_transmission_w
        self._energy_idle_w = energy_idle_w

        # solar energy
        self._solar_trace_enabled = solar_trace_enabled
        self._solar_trace_csv = solar_trace_csv
        self._solar_trace_spec = solar_trace_spec  # type: SolarTraceSpec
        self._solar_trace_csv_col = solar_trace_csv_col
        self._solar_trace_to_watt_multiplier = solar_trace_to_watt_multiplier
        self._solar_trace_efficiency = solar_trace_efficiency

        self._solar_trace_model = SolarPowerModel(
            raw_path=self._solar_trace_csv,
            power_col=self._solar_trace_csv_col,
            scale_x=self._solar_trace_spec.scale_x, scale_y=self._solar_trace_spec.scale_y,
            scale_to_max_x=self._solar_trace_spec.scale_x_max, scale_to_min_x=self._solar_trace_spec.scale_x_min,
            scale_to_max_y=self._solar_trace_spec.scale_y_max, scale_to_min_y=self._solar_trace_spec.scale_y_min,
            cycles=self._solar_trace_spec.cycles, shift=self._solar_trace_spec.shift, parsed_x_limit=None,
            efficiency=self._solar_trace_efficiency
        )

        # traffic
        self._rate_l = rate_l
        """Job arrival rate"""
        self._rate_mu = rate_mu
        """Job execution rate"""
        self._job_payload_size_mb = job_payload_size_mb

        # traffic model
        self._traffic_model = TrafficModel(
            raw_path=rate_l_model_path,
            max_x=simulation_time,
            cycles=rate_l_model_path_cycles,
            shift=rate_l_model_path_shift,
            parsed_x_limit=rate_l_model_path_parse_x_max,
            steady=rate_l_model_path_steady,
            steady_for=rate_l_model_path_steady_for,
            steady_every=rate_l_model_path_steady_every,
        ) if rate_l_model_path is not None else None

        # network
        self._net_speed_client_node_mbits = net_speed_client_node_mbits
        self._net_speed_node_node_mbits = net_speed_node_node_mbits

        # distribution
        self._distribution_network_latency = distribution_network_latency
        self._distribution_network_forwarding_sigma = distribution_network_forwarding_sigma
        # self._distribution_network_latency_cloud_sigma = distribution_network_latency_cloud_sigma
        self._distribution_arrivals = distribution_arrivals
        """The statistical distribution for arrival jobs"""

        #
        # Runtime variables
        #

        # services
        self._service_discovery = discovery  # type: 'DiscoveryService'
        """Discovery service"""
        self._service_data_storage = data_storage  # type: 'DataStorage'
        """Data storage service"""

        # containers
        self._queued_jobs = simpy.resources.container.Container(self._env, capacity=self._max_jobs_in_queue, init=0)
        """Queued jobs container"""
        self._queued_transmission_jobs = simpy.resources.container.Container(self._env, init=0)
        """Queued transmission jobs container"""

        # processes
        # self._process_generators = []
        self._process_job_generator_impl = self._env.process(self._process_jobs_generator())  # type: simpy.Process
        """Process of job generator"""
        self._process_worker_impl = self._env.process(self._process_job_executor())  # type: simpy.Process
        """Process of job processor"""
        self._process_transmission_impl = self._env.process(self._process_job_transmission())  # type: simpy.Process
        self._process_round_impl = self._env.process(self._process_round())  # type: simpy.Process
        self._process_energy_impl = self._env.process(self._process_energy())  # type: simpy.Process
        """Process of job transmission"""
        self._processes_list = [
            self._process_energy_impl,
            self._process_round_impl,
            self._process_job_generator_impl,
            self._process_worker_impl,
            self._process_transmission_impl
        ]
        self._processes_list_to_stop = [
            self._process_round_impl,
            self._process_job_generator_impl,
            self._process_worker_impl,
            self._process_transmission_impl
        ]
        """List of all the active processes"""
        self._jobs_list = []
        """List of jobs in the queue"""
        self._jobs_transmission_list = []
        """List of jobs enqueue for transmission"""
        self._currently_executing_job = None  # type: 'Job' or None
        """The current job that is executing"""

        # counters and lists
        self._total_jobs = 0
        """Total number of dispatched jobs"""
        self._total_processed_job = 0
        """Total number of jobs memorized"""
        self._last_episode_end_at = 0
        """The number of job at which the last episode end"""
        self._scheduled_jobs = {}  # type: Dict
        """List of episode jobs"""
        self._current_episode_number = episode_n
        """Number of total episodes"""
        self._round_n = 0
        """Current round"""
        self._round_last_timestamp = 0
        """Timestamp of the last round"""
        self._round_time_s = round_time_s

        # timings
        self._time_round_total = 0.0
        """Total time employed in task execution+queue from the end to the last round"""
        self._time_round_execution = 0.0
        """Total time employed in task execution from the end to the last round"""
        self._time_round_transmission = 0.0
        """Total time employed in task transmission from the end to the last round"""

        self._time_energy_execution = 0.0
        self._time_energy_transmission = 0.0

        self._time_total_total = 0.0
        self._time_total_execution = 0.0
        self._time_total_transmission = 0.0

        # counters
        self._cnt_round_generated_tasks = 0
        self._cnt_round_arrived_from_neighbours_tasks = 0
        self._cnt_round_rejected_tasks = 0
        self._cnt_round_forwarded_tasks = 0
        self._cnt_round_accepted_tasks = 0

        self._cnt_total_generated_tasks = 0
        self._cnt_total_arrived_from_neighbours_tasks = 0
        self._cnt_total_rejected_tasks = 0
        self._cnt_total_forwarded_tasks = 0
        self._cnt_total_accepted_tasks = 0
        self._cnt_round_lost_tasks = 0

        self._metric_round_energy_consumption = 0.0
        """Energy consumption for current round"""
        self._metric_round_previous_lifespan = 0.0
        """Lifespan of previous round"""
        self._metric_round_previous_energy = 0.0

        # lists for round average
        # self._metric_round_average_every_round_n = 10
        # self._metric_round_rejected_perc_list = []
        self._metric_round_lifespan_seconds_list = []
        self._metric_round_energy_list = []

        # cnt for self and neighbours
        self._cnt_round_list_accepted_tasks = []

        # migration ratios
        self._neighbors = []  # type: List[Node]
        """List of neighbor nodes"""
        self._migration_ratios = []  # type: List[float]
        """Migration ratios towards the neighbors, in order"""
        self._migration_ratio_local = 1.0
        """Migration ratio for tasks to execute locally"""

        # learning
        self._eps_current = eps_initial
        self._eps_initial = eps_initial
        self._eps_decay = eps_decay
        self._eps_min = eps_min

        self._reinforcement_learning_algorithm = reinforcement_learning_algorithm

        self._learning_last_state = self._learning_get_state(default=True)

        self._train_samples = []
        self._train_samples_n = 0
        self._train_every_samples_n = train_every_samples_n

    #
    # Init
    #

    def init(self):
        """Init the node params after services have been installed"""
        # checks
        if self._service_discovery is None:
            raise ValueError("Discovery service cannot be None.")

        # init arrays based upon neighbours
        self._neighbors = self._service_discovery.get_neighbours(current_node_uid=self.get_uid())
        self._migration_ratios = [0.0 for _ in range(len(self._neighbors))]
        self._cnt_round_list_accepted_tasks = [0.0 for _ in range(len(self._neighbors) + 1)]

        Log.minfo(f"{self._module()}", f"Node#{self._uid} completed init for neighbours={len(self._neighbors)}")

    #
    # Processes
    #

    def _process_round(self):
        while True:
            try:
                yield self._env.timeout(self._round_time_s)

                # collect the state
                state = self._learning_get_state()

                # obtain new migration ratios
                new_ratios_l = self._retrieve_new_mratios(state)

                # compute the reward
                # rewards = self._learning_get_reward()

                # train
                # self._reinforcement_learning_algorithm.train(self._learning_last_state, state, rewards, done_episode=end_episode)

                # log round
                self._service_discovery.log_round(originator_node_uid=self.get_uid(),
                                                  episode_n=self._current_episode_number,
                                                  round_n=self._round_n,
                                                  service_data_storage=self._service_data_storage,
                                                  round_time=self._round_time_s)

                # update migration ratios
                self.a_round(new_ratios_l[0], new_ratios_l[1:])

                self._learning_last_state = state

            except Exception as e:
                # traceback.print_tb()
                Log.mwarn(f"{self._module()}", f"_process_round: interrupted, e={e}")
                break

    def _process_jobs_generator(self):
        """Process which generates jobs"""

        Log.mdebug(f"{self._module()}", f"init: _process_jobs_generator")

        while True:
            try:
                # draw time to wait
                time_to_wait = random.expovariate(self._rate_l)
                while time_to_wait <= 0:
                    time_to_wait = random.expovariate(self._rate_l)

                # wait
                yield self._env.timeout(time_to_wait)

                self._total_jobs += 1
                self._cnt_round_generated_tasks += 1
                self._cnt_total_generated_tasks += 1

                # generate a job
                job = Job(self._env,
                          node_uid=self.get_uid(),
                          uid=self._total_jobs,
                          end_clb=self._clb_job_end,
                          payload_size_mbytes=self._job_payload_size_mb,
                          episode=self._current_episode_number,
                          duration=1 / self._rate_mu)
                job.set_learning_state(self._learning_get_state())

                if Node._DEBUG:
                    Log.mdebug(f"{self._module()}", f"_process_jobs_generator: generated {job}")

                # check node if it is alive
                if not self.is_alive():
                    self._cnt_round_lost_tasks += 1
                    job.a_lost()
                    job.a_done()
                    continue

                # append the job to the backlog list
                # self._scheduled_jobs[job.get_uid()] = job

                # add to the transmission queue
                self._job_transmit(job)

            except Exception as e:
                Log.mwarn(f"{self._module()}", f"_process_jobs_generator: interrupted, e={e}")
                break

    def _process_job_transmission(self):
        """Process which simulates a job transmission"""
        if Node._DEBUG:
            Log.mdebug(f"{self._module()}", f"job transmission process started")

        # actual time to
        at = lambda delay, sigma: random.gauss(delay, sigma) if \
            self._distribution_network_latency == Node.DistributionNetworkLatency.GAUSSIAN else delay
        # time to wait
        ttw = lambda j, speed: j.get_payload_size() * 8 / speed  # time-to-wait

        while True:
            # print(self._queued_transmission_jobs.level)
            tt = 0.0
            try:
                # wait for a job
                yield self._queued_transmission_jobs.get(1)
                job = self._jobs_transmission_list.pop(0)
                next_action = job.get_transmission_next_action()

                if Node._DEBUG:
                    Log.mdebug(f"{self._module()}",
                               f"_process_job_transmission: start transmitting job={job}, action={next_action}")

                if next_action == Job.TransmissionAction.CLIENT_TO_NODE_A:
                    # job is sent from the client to the node
                    tt = at(ttw(job, self._net_speed_client_node_mbits), self._distribution_network_forwarding_sigma)
                    yield self._env.timeout(tt)
                    # execute the first decision when arrived
                    self._job_dispatch(job)
                elif next_action == Job.TransmissionAction.NODE_A_TO_NODE_B or next_action == Job.TransmissionAction.NODE_B_TO_NODE_A:
                    # job is transmitted from the scheduler to the worker
                    tt = at(ttw(job, self._net_speed_node_node_mbits), self._distribution_network_forwarding_sigma)
                    yield self._env.timeout(tt)
                    # check the direction
                    if next_action == Job.TransmissionAction.NODE_A_TO_NODE_B:
                        # direction from Node A => Node B
                        self._service_discovery.get_node_by_uid(job.get_forwarded_to_node_id()).schedule_job(job)
                    elif next_action == Job.TransmissionAction.NODE_B_TO_NODE_A:
                        # direction from Node B => Node A
                        originator_node = self._service_discovery.get_node_by_uid(job.get_originator_node_uid())
                        originator_node.job_return_to_client(job)
                    else:
                        raise RuntimeError(f"_process_job_transmission: {next_action} id not valid")
                elif next_action == Job.TransmissionAction.NODE_A_TO_CLIENT:
                    # job is sent from the node to the client
                    tt = at(ttw(job, self._net_speed_client_node_mbits), self._distribution_network_forwarding_sigma)
                    yield self._env.timeout(tt)
                    # remove job from the list of tasks
                    # matching_jobs = [i for i, j in enumerate(self._scheduled_jobs) if j.get_uid() == job.get_uid()]
                    # self._scheduled_jobs[job.get_uid()] = None
                    # declare job end
                    job.a_done()
                    # update counters
                    if not job.is_forwarded():
                        self._cnt_round_list_accepted_tasks[0] += job.get_learning_reward()
                    else:
                        self._cnt_round_list_accepted_tasks[
                            job.get_forwarded_to_node_index() + 1] += job.get_learning_reward()
                    # update total time for jobs
                    self._time_round_total += job.get_total_time()
                    self._time_total_total += job.get_total_time()
                else:
                    Log.merr(f"{self._module()}", f"_process_job_transmission: {job} action not valid")

                if Node._DEBUG:
                    Log.mdebug(f"{self._module()}",
                               f"_process_job_transmission: end "
                               f"job={job} "
                               f"tt={tt} "
                               f"next_action={job.get_transmission_next_action()}, "
                               f"level={self._queued_transmission_jobs.level}")

                # update the total transmission time for the round
                self._time_round_transmission += tt
                self._time_energy_transmission += tt
                self._time_total_transmission += tt

            except Exception as e:
                # traceback.print_tb()
                Log.mwarn(f"{self._module()}", f"_process_job_transmission: interrupted, e={e}")
                break

    def _process_job_executor(self):
        """Process which executes jobs for a node"""
        if Node._DEBUG:
            Log.mdebug(f"{self._module()}", f"job executor started")

        while True:
            try:
                # wait for a job
                yield self._queued_jobs.get(1)
                job = self._jobs_list.pop(0)
                self._currently_executing_job = job  # type: Job

                if not self.is_alive():
                    self._cnt_round_lost_tasks += 1
                    job.a_lost()
                    job.a_done()
                    continue

                # compute the actual job duration according to parameters
                actual_job_duration = Job.compute_duration(self._currently_executing_job,
                                                           Job.DurationType.GAUSSIAN,
                                                           1.0)

                # print(f"rate_mu={self._rate_mu} est_rate={1 / actual_job_duration} fix_rate={job.get_duration()}")

                # execute it
                # yield self._env.timeout(actual_job_duration)
                yield self._env.timeout(1 / self._rate_mu)

                # update exec time
                self._currently_executing_job.a_executed(actual_job_duration)
                self._time_round_execution += actual_job_duration
                self._time_energy_execution += actual_job_duration
                self._time_total_execution += actual_job_duration

                if Node._DEBUG:
                    Log.mdebug(f"{self._module()}",
                               f"_process_job_executor: setting next job={job}, "
                               f"job.is_forwarded={job.is_forwarded()}",
                               f"job.get_transmission_actions_list={job.get_transmission_actions_list()}",
                               )

                # set the next action
                if job.is_forwarded():
                    # return to originator node
                    self._job_return_to_originator_node(job)
                else:
                    # return to client
                    self.job_return_to_client(job)

                # read to the transmission queue
                # self._job_transmit(job)

                if Node._DEBUG:
                    Log.mdebug(f"{self._module()}",
                               f"_process_job_executor: executed job={job} "
                               f"actual_job_duration={actual_job_duration} "
                               f"next_action={job.get_transmission_next_action()} "
                               f"level={self._queued_jobs.level} "
                               f"job={self._currently_executing_job} "
                               f"job.is_forwarded={self._currently_executing_job.is_forwarded()} "
                               f"job.originator={self._currently_executing_job.get_originator_node_uid()} "
                               f"job.forwarded_to={self._currently_executing_job.get_forwarded_to_node_id()}"
                               )

                # clear currently executing
                self._currently_executing_job = None

            except Exception as e:
                Log.mwarn(f"{self._module()}", f"_process_job_executor: interrupted, e={e}")
                Log.mwarn(f"{self._module()}",
                          f"_process_job_executor: "
                          f"next_action={self._currently_executing_job.get_transmission_next_action() if self._currently_executing_job is not None else None} "
                          f"job={self._currently_executing_job} "
                          f"job.is_forwarded={self._currently_executing_job.is_forwarded() if self._currently_executing_job is not None else None} "
                          f"job.originator={self._currently_executing_job.get_originator_node_uid() if self._currently_executing_job is not None else None} "
                          f"job.forwarded_to={self._currently_executing_job.get_forwarded_to_node_id() if self._currently_executing_job is not None else None}"
                          )
                break

    def _process_energy(self):
        if Node._DEBUG:
            Log.mdebug(f"{self._module()}", f"_process_energy: started")

        while True:
            try:
                yield self._env.timeout(1)

                # discharge the battery
                to_discharge_ws = self._compute_battery_discharge(
                    1.0,
                    self._time_energy_execution,
                    self._time_energy_transmission)

                to_charge_ws = 0.0
                # recharge according to the solar model
                if self._solar_trace_enabled:
                    power_at_now = self._solar_trace_model.get_power_at(self._env.now)
                    to_charge_ws = (power_at_now * self._solar_trace_to_watt_multiplier) / 3600

                # self._battery_current_capacity_wh += to_charge_ws

                # reset
                self._time_energy_execution = .0
                self._time_energy_transmission = .0

                if self._stop_when_node_shutdown and self.get_battery_residual_percentage() <= 0.0:
                    Log.minfo(self._module(),
                              f"_process_round: ep. ended, residual_battery_p={self.get_battery_residual_percentage()}")
                    self.a_stop()
                    # self._service_discovery.end_episode(self.get_uid(),
                    #                                    episode_n=self._current_episode_number,
                    #                                    lifespan_min=self._env.now,
                    #                                    service_data_storage=self._service_data_storage)

                    break

                # save lifespan at second
                net_energy = to_charge_ws - to_discharge_ws
                # self._metric_round_lifespan_seconds_list.append(self.get_lifespan_last_second())
                self._metric_round_energy_list.append(to_discharge_ws)

                # update battery
                self._battery_current_capacity_wh += net_energy

                if self._battery_current_capacity_wh > self._battery_total_capacity_wh:
                    self._battery_current_capacity_wh = self._battery_total_capacity_wh

                # negative battery falls to zero
                if self._battery_current_capacity_wh < 0:
                    self._battery_current_capacity_wh = 0.0

                if Node._DEBUG:
                    Log.mdebug(self._module(), f"_process_round: "
                                               f"t={self._env.now} "
                                               f"current_battery={self._battery_current_capacity_wh} "
                                               f"to_discharge_ws={to_discharge_ws} "
                                               f"to_charge_ws={to_charge_ws}")

                # if self._env.now > 200:
                #     exit(1)

            except Exception as e:
                Log.mwarn(f"{self._module()}", f"_process_energy: interrupted, e={e}")

    #
    # Core
    #

    def _act_on_migration(self) -> (SchedulingAction, int):
        """Make a scheduling decision based on the array of migration ratios"""
        action = Node.SchedulingAction.EXECUTE_LOCALLY
        action_to = -1

        rnd = random.random()
        if rnd < self._migration_ratio_local:
            action = Node.SchedulingAction.EXECUTE_LOCALLY
            action_to = -1
            # Log.mdebug(f"{self._module()}", f"_act_on_migration: action={action.name}, action_to={action_to}")
            return action, action_to

        cumulative_probability = self._migration_ratio_local
        for i, ratio in enumerate(self._migration_ratios):
            cumulative_probability += ratio
            if rnd < cumulative_probability:
                action = Node.SchedulingAction.FORWARD_TO_NEIGHBOUR
                action_to = i
                self._cnt_round_forwarded_tasks += 1
                self._cnt_total_forwarded_tasks += 1
                # Log.mdebug(f"{self._module()}", f"_act_on_migration: action={action.name}, action_to={action_to}")
                return action, action_to

        # self._cnt_total_forwarded_tasks += 1
        # action = Node.SchedulingAction.FORWARD_TO_NEIGHBOUR
        # action_to = len(self._migration_ratios) - 1

        # Log.mdebug(f"{self._module()}", f"_act_on_migration: action={action.name}, action_to={action_to}")
        Log.mfatal(Node._MODULE, "_act_on_migration: cumulative probability exceeds")
        return action, action_to

    def _retrieve_new_mratios(self, state) -> List[float]:
        new_ratios = []
        if self._scheduler_type == Node.SchedulerType.REINFORCEMENT_LEARNING:
            new_ratios, _ = self._reinforcement_learning_algorithm.predict(state)
            new_ratios = new_ratios.tolist()

        if self._scheduler_type == Node.SchedulerType.FIXED_POLICY:
            if self._scheduler_fixed_policy_algorithm == Node.SchedulerFixedPolicyAlgorithm.RATIO_ADAPTIVE_STEP:
                new_ratios = self._retrieve_new_mratios_adaptive_step()

            if self._scheduler_fixed_policy_algorithm == Node.SchedulerFixedPolicyAlgorithm.NO_SCHEDULER:
                new_ratios = [1.0] + [0.0 for _ in range(len(self._service_discovery.get_neighbours()))]

        if round(sum(new_ratios), 2) > 1.0:
            Log.mfatal(self._module(), f"Migration ratios are exceeding 1: {new_ratios}")

        if Node._DEBUG:
            Log.mdebug(self._module(), f"_retrieve_new_mratios: set ratios are {new_ratios}")

        return new_ratios

    def _retrieve_new_mratios_adaptive_step(self) -> List[float]:
        new_migration_ratios = [m for m in self.get_migration_ratios_neighbors()]  # type: List[float]

        # retrieve the stats of all neighbors and self
        neighbors_lifespan = []
        neighbors_battery = []

        for n in self._neighbors:
            neighbors_lifespan.append(n.get_lifespan_last_round())
            neighbors_battery.append(n.get_battery_residual_percentage())

        # Log.mdebug(f"{self._module()}", f"[t={self._env.now}] last_load={self.get_last_latency():.2f} others={loads}")

        ratios_total = sum(self.get_migration_ratios_neighbors())

        def can_be_given_to_node(how_much, node_i):
            remote_rejecting_rate = self._service_discovery.get_neighbours()[i].get_perc_round_rejected()
            is_rejecting_tasks = remote_rejecting_rate > 0.0
            is_rejecting_less = remote_rejecting_rate < self.get_perc_round_rejected()
            return round(ratios_total + how_much, 2) <= 1.0 and (not is_rejecting_tasks or is_rejecting_less)
            # return round(ratios_total + how_much, 2) <= 1.0

        def can_be_subtract(how_much):
            return round(ratios_total - how_much, 2) >= 0.0

        def can_be_subtract_to_node(how_much, node_i):
            return round(new_migration_ratios[node_i] - how_much, 2) >= 0.0

        # compute average latency
        average_lifespan = (sum(neighbors_lifespan) + self.get_lifespan_last_round()) / (1 + len(self._neighbors))
        average_lifespan_low = .95 * average_lifespan
        average_lifespan_high = 1.05 * average_lifespan

        def delta(to_node_i):
            return 0.01

        if Node._DEBUG:
            Log.mdebug(f"{self._module()}", f"[t={self._env.now}] _retrieve_new_mratios_adaptive_step: B "
                                            f"average_lifespan=L={average_lifespan_low:.3f},A={average_lifespan:.3f},H={average_lifespan_high:.3f} "
                                            f"self.get_lifespan_round()={self.get_lifespan_last_round():.3f} "
                                            f"neighbors_lifespan={neighbors_lifespan} "
                                            f"ratios={new_migration_ratios} "
                                            f"sum(ratios)={ratios_total:.3f}")

        if self.get_lifespan_last_round() >= average_lifespan_high:
            # if I am giving something to others then reduce them all
            if ratios_total > 0.0:
                for i in range(len(self._neighbors)):
                    if can_be_subtract_to_node(delta(i), i) and can_be_subtract(delta(i)):
                        new_migration_ratios[i] = round(new_migration_ratios[i] - delta(i), 2)
                        ratios_total = round(ratios_total - delta(i), 2)
                return [1 - sum(new_migration_ratios)] + new_migration_ratios

        # if less than the average then I have to give others
        if self.get_lifespan_last_round() <= average_lifespan_low:
            for i, n in enumerate(self._neighbors):
                if n.get_lifespan_last_round() <= average_lifespan_low:
                    if can_be_subtract_to_node(delta(i), i) and can_be_subtract(delta(i)):
                        new_migration_ratios[i] = round(new_migration_ratios[i] - delta(i), 2)
                        ratios_total = round(ratios_total - delta(i), 2)

                if n.get_lifespan_last_round() >= average_lifespan_high:
                    if can_be_given_to_node(delta(i), i):
                        new_migration_ratios[i] = round(new_migration_ratios[i] + delta(i), 2)
                        ratios_total = round(ratios_total + delta(i), 2)

        if Node._DEBUG:
            Log.mdebug(f"{self._module()}", f"[t={self._env.now}] _retrieve_new_mratios_adaptive_step: A "
                                            f"average_lifespan=L={average_lifespan_low:.3f},A={average_lifespan:.3f},H={average_lifespan_high:.3f} "
                                            f"self.get_lifespan_round()={self.get_lifespan_last_round():.3f} "
                                            f"neighbors_lifespan={neighbors_lifespan} "
                                            f"ratios={new_migration_ratios} "
                                            f"sum(ratios)={ratios_total:.3f}")

        return [1 - sum(new_migration_ratios)] + new_migration_ratios

    #
    # Core -> Actions
    #

    def _job_forward_to_node(self, job, to_neighbour_index: int):
        to_node = self._neighbors[to_neighbour_index]

        # Log.minfo(self._module(), f"{self._neighbors[to_neighbour_index].get_uid()})")

        job.set_transmission_next_action(Job.TransmissionAction.NODE_A_TO_NODE_B)
        job.a_forwarded(to_node_uid=to_node.get_uid(), to_node_index=to_neighbour_index)
        self._job_transmit(job)

    # def _job_forward_to_cloud(self, job):
    #    job.set_transmission_next_action(Job.TransmissionAction.SCHEDULER_TO_CLOUD)
    #    job.a_forwarded(to_cloud=True)
    #    self._job_transmit(job)

    def job_return_to_client(self, job):
        """Re-add to transmission queue for re-trasmit the node to the client. This is called after a job is executed
        remotely"""
        if self.get_uid() != job.get_originator_node_uid():
            raise RuntimeError("job_return_to_client: current node cannot return to client since it is not the origin")

        job.set_transmission_next_action(Job.TransmissionAction.NODE_A_TO_CLIENT)
        self._job_transmit(job)

    def _job_return_to_originator_node(self, job):
        """Re-add to transmission queue for re-trasmit the node to the client. This is called after a job is executed
        remotely"""
        if self.get_uid() != job.get_forwarded_to_node_id():
            raise RuntimeError("_job_return_to_originator_node: this is not the forwarded node, so there is an error")

        job.set_transmission_next_action(Job.TransmissionAction.NODE_B_TO_NODE_A)
        self._job_transmit(job)

    def _job_transmit(self, job):
        """Schedule a job for transmission, direction is written in job.next_action"""
        self._jobs_transmission_list.append(job)
        self._queued_transmission_jobs.put(1)

    def _job_schedule(self, job):
        """Schedule a job internally in the node"""
        # if node is full loaded
        if self.get_current_load() >= self._max_jobs_in_queue:
            self._job_reject(job, Job.RejectionCause.NODE_SATURATED)
            return False

        self._job_accept(job)
        return True

    def _job_accept(self, job: Job):
        self._cnt_round_accepted_tasks += 1
        self._cnt_total_accepted_tasks += 1

        self._jobs_list.append(job)
        self._queued_jobs.put(1)

        job.a_in_queue()

    def _job_reject(self, job: Job, rejection_cause: Job.RejectionCause):
        self._cnt_round_rejected_tasks += 1
        self._cnt_total_rejected_tasks += 1

        job.a_rejected(rejection_cause)

        if self.get_uid() == job.get_originator_node_uid():
            self.job_return_to_client(job)
        elif self.get_uid() == job.get_forwarded_to_node_id():
            self._job_return_to_originator_node(job)
            # job.set_transmission_next_action(Job.TransmissionAction.NODE_TO_NODE)
            # self._job_transmit(job)
        else:
            raise RuntimeError("_job_reject: current node cannot reject the job")

    def _job_dispatch(self, job):
        """Decide what to do with the newly come job"""
        # mark as dispatched
        job.a_dispatched()

        # make decision based on migration ratios
        action, to_neighbour_index = self._act_on_migration()
        if action == Node.SchedulingAction.EXECUTE_LOCALLY:
            job.set_learning_action(0)
            self.schedule_job(job)
        elif action == Node.SchedulingAction.FORWARD_TO_NEIGHBOUR:
            job.set_learning_action(to_neighbour_index + 1)
            self._job_forward_to_node(job, to_neighbour_index=to_neighbour_index)

    #
    # Learning
    #

    def _learning_get_state(self, default=False) -> List[float]:
        if default:
            return [100.0, 0.0, 100.0, 0]

        return [self.get_battery_residual_percentage(),
                self.get_requests_rate_effective(),
                self.get_lifespan_last_round(),
                self._queued_jobs.level]

    def _learning_get_reward(self):
        reward = []

        current_node_i = -1
        all_lifespans = []
        for i, node in enumerate(self._service_discovery.get_all_nodes_sorted()):
            if node.get_uid() == self.get_uid():
                current_node_i = i

            all_lifespans.append(node.get_lifespan_round())

        avg_lifespan_t1 = sum(all_lifespans) / len(all_lifespans)

        i_in_range = avg_lifespan_t1 * .95 <= all_lifespans[current_node_i] <= avg_lifespan_t1 * 1.05
        i_above_range = all_lifespans[current_node_i] > avg_lifespan_t1 * 1.05
        i_below_range = all_lifespans[current_node_i] < avg_lifespan_t1 * .95

        if i_in_range:
            reward.append(0)
        elif i_above_range:
            reward.append(-1)
        elif i_below_range:
            reward.append(1)
        else:
            Log.mfatal(self._module(), f"case not addressed: i_in_range={i_in_range} "
                                       f"i_above_range={i_above_range} "
                                       f"i_below_range={i_below_range} "
                                       f"avg_lifespan_t1={avg_lifespan_t1} "
                                       f"all_lifespans[current_node_i]={all_lifespans[current_node_i]}")

        for j in range(len(self._service_discovery.get_all_nodes_sorted())):
            if current_node_i == j:
                continue

            j_in_range = avg_lifespan_t1 * .95 <= all_lifespans[j] <= avg_lifespan_t1 * 1.05
            j_above_range = all_lifespans[j] > avg_lifespan_t1 * 1.05
            j_below_range = all_lifespans[j] < avg_lifespan_t1 * .95

            if i_above_range and j_above_range:
                reward.append(-1)
            elif i_above_range and j_in_range:
                reward.append(-1)
            elif i_above_range and j_below_range:
                reward.append(-1)
                # i below
            elif i_below_range and j_above_range:
                reward.append(1)
            elif i_below_range and j_in_range:
                reward.append(-1)
            elif i_below_range and j_below_range:
                reward.append(-1)
                # i in range
            elif i_in_range and j_above_range:
                reward.append(1)
            elif i_in_range and j_in_range:
                reward.append(0)
            elif i_in_range and j_below_range:
                reward.append(-1)
            else:
                Log.mfatal(self._module(), f"case not addressed: i_in_range={i_in_range} "
                                           f"j_in_range={j_in_range} "
                                           f"i_above_range={i_above_range} "
                                           f"j_above_range={j_above_range} "
                                           f"i_below_range={i_below_range} "
                                           f"j_below_range={j_below_range}")

        return reward

    #
    # Callbacks
    #

    def _clb_job_end(self, job):
        """Called when a job ends by the generator node, executed or rejected"""

        if self._scheduler_type == Node.SchedulerType.REINFORCEMENT_LEARNING:
            # set the reward
            if job.is_rejected():
                job.set_learning_reward(-1)

            if job.is_executed():
                if job.is_forwarded():
                    if job.get_lifespan_originator() <= job.get_lifespan_destination():
                        job.set_learning_reward(1)
                    else:
                        job.set_learning_reward(-1)
                else:
                    job.set_learning_reward(0)

            # add to training samples
            self._train_samples.append(TrainingSample(
                state=job.get_learning_state(),
                action=job.get_learning_action(),
                reward=job.get_learning_reward()
            ))
            self._train_samples_n += 1

            # start training if needed
            if self._train_samples_n > self._train_every_samples_n:
                batch_reward = sum([e.reward for e in self._train_samples])

                # train the model
                loss_actor, loss_critic = self._reinforcement_learning_algorithm.train(self._train_samples)

                if Node._DEBUG:
                    Log.mdebug(self._module(), f"_clb_job_end: started training, "
                                               f"ep={self._current_episode_number} "
                                               f"r={self._round_n} "
                                               f"loss_actor={loss_actor} "
                                               f"loss_critic={loss_critic} "
                                               f"batch_reward={batch_reward}")

                self._train_samples_n = 0
                self._train_samples = []

        if Node._DEBUG:
            Log.mdebug(f"{self._module()}", f"_clb_job_end: j={job}, episode #{job.get_episode()}")

    #
    # Logging
    #

    def _log_episode_data(self, episode, eps, score, total_jobs, loss, mse, mae):
        self._service_data_storage.done_episode(self._uid, episode, eps, score, total_jobs, loss, mse, mae)

    def _module(self):
        return f"{Node._MODULE}#{self.get_uid()}"

    #
    # Exported methods
    #

    def get_configuration_dict(self) -> dict:
        return {
            "node_uid": self.get_uid(),
            "rate_l": self._rate_l,
            "rate_mu": self._rate_mu,
            "max_jobs_in_queue": self._max_jobs_in_queue,

            "scheduler_type": self._scheduler_type.name,
            "scheduler_algorithm": self._scheduler_fixed_policy_algorithm.name,

            "net_speed_client_node_mbits": self._net_speed_client_node_mbits,
            "net_speed_node_node_mbits": self._net_speed_node_node_mbits,

            "job_payload_size_mb": self._job_payload_size_mb,

            "distribution_arrivals": self._distribution_arrivals.name,
            "distribution_network_latency": self._distribution_network_latency.name,

            "battery_total_capacity_wh": self._battery_total_capacity_wh,
            "battery_initial_capacity_wh": self._battery_initial_capacity_wh,
            "energy_cpu_w": self._energy_cpu_w,
            "energy_transmission_w": self._energy_transmission_w,
            "energy_idle_w": self._energy_idle_w,

            # solar
            "solar_trace_enabled": self._solar_trace_enabled,
            "solar_trace_spec": self._solar_trace_spec.get_obj(),
            "solar_trace_csv": self._solar_trace_csv,
            "solar_trace_to_watt_multiplier": self._solar_trace_to_watt_multiplier
        }

    def set_service_discovery(self, service_discovery):
        self._service_discovery = service_discovery

        # check if init should be executed
        if self._service_data_storage is not None and self._service_discovery is not None:
            self.init()

    def set_service_data_storage(self, service_data_storage):
        self._service_data_storage = service_data_storage

        # check if init should be executed
        if self._service_data_storage is not None and self._service_discovery is not None:
            self.init()

    def schedule_job(self, job: Job):
        """Schedule a job from external, without adding to the transmission queue, since only who sends is in charge
        of the transmission"""
        if Node._DEBUG:
            Log.mdebug(f"{self._module()}", f"schedule_job: scheduling j={job}")

        if job.is_forwarded():
            self._cnt_round_arrived_from_neighbours_tasks += 1
            self._cnt_total_arrived_from_neighbours_tasks += 1

        return self._job_schedule(job)

    def return_job_from_cloud(self, job):
        """Re-add to transmission queue for re-trasmit from cloud to scheduler. This is called after a job is executed
        in the cloud"""
        self._job_transmit(job)

    def is_running_job(self):
        return self._currently_executing_job is not None

    def get_current_load(self) -> int:
        """Retrieve the number of queued tasks"""
        return self._queued_jobs.level + 1 if self._currently_executing_job is not None else 0

    def get_uid(self):
        """Return the identifier number of the node"""
        return self._uid

    def get_job_list(self):
        return self._jobs_list

    def get_currently_executing_job(self):
        return self._currently_executing_job

    def get_max_queue_length(self):
        return self._max_jobs_in_queue

    def get_battery_residual(self) -> float:
        return self._battery_current_capacity_wh

    def get_battery_residual_percentage(self) -> float:
        return self._battery_current_capacity_wh / self._battery_total_capacity_wh

    def get_time_execution_round(self) -> float:
        return self._time_round_execution

    def get_time_transmission_round(self) -> float:
        return self._time_round_transmission

    def get_operations_time_round(self) -> (float, float):
        return self._time_round_execution, self._time_round_transmission

    def get_operations_battery_discharge_round(self) -> (float, float, float):
        return self._compute_battery_discharge_split(self._time_round_total,
                                                     self._time_round_execution,
                                                     self._time_round_transmission)

    def get_perc_total_rejected(self):
        total_jobs = self._cnt_total_generated_tasks + self._cnt_total_arrived_from_neighbours_tasks
        if total_jobs == 0:
            return 0

        return self._cnt_total_rejected_tasks / total_jobs

    def get_perc_total_forwarded(self):
        if self._cnt_total_generated_tasks == 0:
            return 0

        return self._cnt_total_forwarded_tasks / self._cnt_total_generated_tasks

    def get_perc_round_rejected(self, plus_lost=True):
        total = self._cnt_round_generated_tasks + self._cnt_round_arrived_from_neighbours_tasks
        if total == 0:
            return 0.0

        total_rejected = self._cnt_round_rejected_tasks
        if plus_lost:
            total_rejected += self._cnt_round_lost_tasks

        return total_rejected / total

    def get_perc_round_forwarded(self):
        if self._cnt_round_generated_tasks == 0:
            return 0

        return self._cnt_round_forwarded_tasks / self._cnt_round_generated_tasks

    def get_lifespan_last_round(self):
        if self._metric_round_previous_energy <= 0.0:
            return 0.0
        if self._battery_last_round_capacity_wh <= 0.0:
            return 0.0

        return self._battery_last_round_capacity_wh / (self._metric_round_previous_energy * 3600.0)

    def get_power_last_round_w(self):
        if self._battery_last_round_capacity_wh <= 0.0:
            return 0.0

        return self._metric_round_previous_energy * 3600

    def get_requests_rate(self):
        return self._cnt_round_generated_tasks / self._round_time_s

    def get_requests_rate_effective(self):
        return (
                self._cnt_round_generated_tasks + self._cnt_round_arrived_from_neighbours_tasks - self._cnt_round_forwarded_tasks) / self._round_time_s

    def get_battery_discharge_round(self):
        return self._metric_round_energy_consumption / self._round_time_s

    def get_consumption_full_load(self):
        return self._compute_battery_discharge(self._round_time_s, self._round_time_s, self._round_time_s)

    def get_round_list_accepted_tasks(self):
        return self._cnt_round_list_accepted_tasks

    def get_migration_ratios(self) -> (float, List[float]):
        """Retrieve the current migration ratios, local and to neighbor"""
        return self._migration_ratio_local, self._migration_ratios

    def get_migration_ratio_local(self) -> float:
        """Retrieve the neighbors migration ratios"""
        return self._migration_ratio_local

    def get_migration_ratios_neighbors(self) -> (List[float]):
        """Retrieve the neighbors migration ratios"""
        return self._migration_ratios

    def is_alive(self):
        """Get if the current node can execute tasks"""
        return self.get_battery_residual_percentage() > 0.0

    #
    # Setters
    #

    def _set_migration_ratios(self, new_ratios):
        if len(new_ratios) != len(self._neighbors):
            raise RuntimeError("Passed ratios does not match the neighbours list of the node")
        self._migration_ratios = new_ratios

    #
    # Actions
    #

    def a_round(self, new_locally_executed_ratio=1.0, new_migration_ratios=None):
        """Triggers the new round"""

        if Node._DEBUG:
            Log.mdebug(f"{self._module()}",
                       f"a_round: battery_current_capacity_wh={self._battery_current_capacity_wh:.4f} "
                       f"round_last_timestamp={self._round_last_timestamp} "
                       f"time_execution_round={self._time_round_execution:.4f} "
                       f"time_transmission_round={self._time_round_transmission:.4f} "
                       f"total_discharge={self._metric_round_energy_consumption:.4f} "
                       f"now={self._env.now}")

        # update last counters
        # save lifespan last round
        self._metric_round_previous_energy = sum(self._metric_round_energy_list) / self._round_time_s
        self._metric_round_energy_list = []
        # self._metric_round_previous_lifespan = self.get_lifespan_last_round()  # sum(self._metric_round_lifespan_seconds_list) / self._round_time_s
        # self._metric_round_lifespan_seconds_list = []

        # reset timings and counters
        self._time_round_total = 0.0
        self._time_round_execution = 0.0
        self._time_round_transmission = 0.0

        self._cnt_round_generated_tasks = 0
        self._cnt_round_arrived_from_neighbours_tasks = 0
        self._cnt_round_rejected_tasks = 0
        self._cnt_round_lost_tasks = 0
        self._cnt_round_forwarded_tasks = 0
        self._cnt_round_accepted_tasks = 0

        self._cnt_round_list_accepted_tasks = [0 for _ in range(len(self._neighbors) + 1)]

        # update migration ratios
        if new_migration_ratios is not None:
            self._migration_ratio_local = new_locally_executed_ratio
            self._set_migration_ratios(new_migration_ratios)
            if round(self._migration_ratio_local + sum(self._migration_ratios), 2) != 1.0:
                raise RuntimeError(f"{self._module()}: a_round: "
                                   f"ratios != 1.0 => {self._migration_ratio_local + sum(self._migration_ratios)}")

        # increase rounds count
        self._round_n += 1
        self._round_last_timestamp = self._env.now

        self._battery_last_round_capacity_wh = self._battery_current_capacity_wh

    def a_stop(self, originator=False):
        """Stop all the processes"""
        # self._process_job_generator_impl.interrupt()
        # self._process_worker_impl.interrupt()

        for process in self._processes_list_to_stop:
            if process.is_alive:
                process.interrupt()

        # if not originator:
        #    self._process_round_impl.interrupt()

    #
    # Utils
    #

    def _compute_battery_discharge_split(self, time_idle, time_execution, time_transmission) -> (float, float, float):
        discharge_idle = time_idle * (self._energy_idle_w / 3600)
        discharge_execution = time_execution * (self._energy_cpu_w / 3600)
        discharge_transmission = time_transmission * (self._energy_transmission_w / 3600)

        return discharge_idle, discharge_execution, discharge_transmission

    def _compute_battery_discharge(self, time_idle, time_execution, time_transmission):
        discharge_idle, discharge_execution, discharge_transmission = self._compute_battery_discharge_split(time_idle,
                                                                                                            time_execution,
                                                                                                            time_transmission)

        return discharge_idle + discharge_execution + discharge_transmission
