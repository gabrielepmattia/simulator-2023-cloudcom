#  Copyright (c) 2023. Gabriele Proietti Mattia <pm.gabriele@gmail.com>
#  simulator-2023-mswim - Simpy simulator of energy schedulers
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.

from run_experiment_fixed_policy import RunExperiment
from node import Node
from plot_metrics import PlotExperiment

folder_no_scheduler = RunExperiment.main(Node.SchedulerFixedPolicyAlgorithm.NO_SCHEDULER)
folder_adaptive = RunExperiment.main(Node.SchedulerFixedPolicyAlgorithm.RATIO_ADAPTIVE_STEP)

folders = [folder_no_scheduler, folder_adaptive]

# plot

# PlotUtils.use_tex()

for folder in folders:
    print(f"Plotting {folder}...")
    e = PlotExperiment(exp_folder=folder)
    e.run()
    e.close()