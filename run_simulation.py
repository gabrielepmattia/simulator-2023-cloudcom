#  Copyright (c) 2023. Gabriele Proietti Mattia <pm.gabriele@gmail.com>
#  simulator-2023-cloudcom - Simpy simulator of energy schedulers
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.

from __future__ import annotations

import signal
import sys
from datetime import datetime

import simpy

from log import Log
from node import Node
from orchestrator import Orchestrator
from service_data_storage import ServiceDataStorage
from service_discovery import ServiceDiscovery

"""
Run the simulation of deadline scheduling
"""

MODULE = "Main"

SIMULATION_TIME = 1e10
SIMULATION_TOTAL_TIME = SIMULATION_TIME

SESSION_ID = datetime.now().strftime("%Y%m%d-%H%M%S")


def simulate(env):
    nodes = []

    # cloud = Cloud(env, latency_roundtrip_ms=20)
    orchestrator = Orchestrator(env)

    # create nodes
    nodes.append(create_node(env, 0, 2, 15))
    nodes.append(create_node(env, 1, 3, 15))
    nodes.append(create_node(env, 2, 4, 15))
    nodes.append(create_node(env, 3, 5, 15))
    nodes.append(create_node(env, 4, 6, 15))

    topology = {
        0: [1, 2, 3, 4],
        1: [0, 2, 3, 4],
        2: [0, 1, 3, 4],
        3: [0, 1, 2, 4],
        4: [0, 1, 2, 3],
    }

    # add them discovery service
    discovery = ServiceDiscovery(nodes, topology)
    data_storage = ServiceDataStorage(nodes, SESSION_ID)

    # init nodes services, and data
    for node in nodes:
        node.set_service_discovery(discovery)
        node.set_service_data_storage(data_storage)
    orchestrator.set_service_discovery(discovery)
    orchestrator.set_service_data_storage(data_storage)

    Log.minfo(MODULE, "Started simulation")
    env.run(until=SIMULATION_TOTAL_TIME)
    Log.minfo(MODULE, f"Simulation ended: SESSION_ID={SESSION_ID}")

    data_storage.done_simulation()


def create_node(env, node_id, rate_l, rate_mu):
    return Node(env, node_id, SESSION_ID,
                simulation_time=SIMULATION_TIME,
                # rates
                rate_l=rate_l,
                # traffic model
                # rate_l_model_path="./traffic/namex/namex-traffic-daily-20210420.csv",
                # rate_l_model_path=f"./traffic/fixed/fixed_{i}.csv",
                # rate_l_model_path=f"./traffic/city/data/traffic_node_{i}.csv",
                # rate_l_model_path="./traffic/fictious/fictious_1.csv",
                rate_l_model_path_shift=0,  # i * 1200,  # 0,
                rate_l_model_path_cycles=3,
                rate_l_model_path_parse_x_max=None,
                rate_l_model_path_steady=False,
                rate_l_model_path_steady_for=2000,
                rate_l_model_path_steady_every=2000,
                rate_mu=rate_mu,
                # net
                net_speed_client_node_mbits=200,
                net_speed_node_node_mbits=300,
                # node info
                max_jobs_in_queue=5,
                distribution_arrivals=Node.DistributionArrivals.POISSON,
                # energy
                battery_capacity_wh=10,
                energy_cpu_w=2.97,
                energy_transmission_w=0.015,
                energy_idle_w=2.5,
                # distributions
                distribution_network_probing_sigma=0.0001,
                distribution_network_forwarding_sigma=0.00002,
                )


def main(argv):
    env = simpy.Environment()
    simulate(env)


#
# Signals
#

def signal_handler(signal, frame):
    Log.minfo(MODULE, "Interrupt received, closing gracefully")
    sys.exit(0)


signal.signal(signal.SIGINT, signal_handler)

#
# Entrypoint
#

if __name__ == "__main__":
    main(sys.argv)

    # import cProfile
    # cProfile.run('main(sys.argv)')
