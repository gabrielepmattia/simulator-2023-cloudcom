#  Copyright (c) 2023. Gabriele Proietti Mattia <pm.gabriele@gmail.com>
#  simulator-2023-cloudcom - Simpy simulator of energy schedulers
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.

from utils import Utils


def reward_fn_avg_lifespan(avg_hrs_list, max_diff=0.0, max_reward=200):
    if max_diff < 1e-2:
        return max_reward

    if min(avg_hrs_list) < 0:
        m, q = Utils.line_by_two_points(-max_diff, 0, -max_reward, 0)
        r = m * avg_hrs_list + q
        return r

    avg_hrs = sum(avg_hrs_list) / len(avg_hrs_list)
    if avg_hrs < 0:
        m, q = Utils.line_by_two_points(-max_diff, 0, -max_reward, 0)
        r = m * avg_hrs + q
        return r

    m, q = Utils.line_by_two_points(0, max_diff, max_reward, 0)
    r = m * avg_hrs + q

    return r


def reward_fn_diff_lifespan(diff_hrs, max_diff=0.0, max_reward=200):
    if max_diff < 1e-2:
        return max_reward

    if diff_hrs < 0:
        m, q = Utils.line_by_two_points(-max_diff, 0, -max_reward, 0)
        r = m * diff_hrs + q
        return r

    m, q = Utils.line_by_two_points(0, max_diff, max_reward, 0)
    r = m * diff_hrs + q

    return r

def reward_fn_exchanged_tasks(diff_tasks, i=None, j=None, max_tasks=0.0, max_reward=200):
    if max_tasks < 1e-2:
        return max_reward

    m, q = Utils.line_by_two_points(0, max_tasks, max_reward, 0)
    # r = max_sec_tolerance * math.pow(diff_tasks, 2)
    # return r if r <= 200.0 else 200.0

    # if i is not None and j is not None:
    #     t_ij = self._last_migration_m[i][j] * self._last_state_m[1][i]
    #     t_ji = self._last_migration_m[j][i] * self._last_state_m[1][j]

    #     if t_ij > t_ji:
    #         return m * diff_tasks + q
    #     else:
    #         return 0.0
    # else:
    return m * diff_tasks + q

print(reward_fn_diff_lifespan(diff_hrs=-3, max_diff=2.0, max_reward=200))
print(reward_fn_diff_lifespan(diff_hrs=-0.7, max_diff=2.0, max_reward=200))
print(reward_fn_diff_lifespan(diff_hrs=-0.5, max_diff=2.0, max_reward=200))
print(reward_fn_diff_lifespan(diff_hrs=0, max_diff=2.0, max_reward=200))
print(reward_fn_diff_lifespan(diff_hrs=0.5, max_diff=2.0, max_reward=200))
print(reward_fn_diff_lifespan(diff_hrs=1, max_diff=2.0, max_reward=200))
print(reward_fn_diff_lifespan(diff_hrs=1.5, max_diff=2.0, max_reward=200))
print()
print(reward_fn_exchanged_tasks(diff_tasks=0.0, max_tasks=30, max_reward=200))
print(reward_fn_exchanged_tasks(diff_tasks=5.0, max_tasks=30, max_reward=200))
print(reward_fn_exchanged_tasks(diff_tasks=10.0, max_tasks=30, max_reward=200))
print(reward_fn_exchanged_tasks(diff_tasks=15.0, max_tasks=30, max_reward=200))
print(reward_fn_exchanged_tasks(diff_tasks=20.0, max_tasks=30, max_reward=200))