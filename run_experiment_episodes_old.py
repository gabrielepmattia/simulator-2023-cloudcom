#  Copyright (c) 2023. Gabriele Proietti Mattia <pm.gabriele@gmail.com>
#  simulator-2023-cloudcom - Simpy simulator of energy schedulers
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.

import sys

from best import Best
from log import Log
from reinforcement import ActorCritic
from service_data_storage import ServiceDataStorage
from simulate import Simulate
from utils import Utils


class ExperimentEpisodes:
    _MODULE = "ExperimentEpisodes"

    def __init__(self, service_data_storage: ServiceDataStorage, reinforcement_algorithm_array=None, max_episodes=1,
                 best: Best = None,
                 round_time_s=30):
        self._max_episodes = int(max_episodes)
        """Max number of episodes"""
        self._service_data_storage = service_data_storage
        """Data storage service for storing results"""
        self._best = best
        """Best object for keeping the best results in memory"""
        self._round_time_s = round_time_s
        """Round time when nodes updates the migration ratios"""

        self._reinforcement_algorithm_array = reinforcement_algorithm_array
        """Array of RL algorithm, one for each node"""

    def run(self):
        for i in range(self._max_episodes):
            sim = Simulate(service_data_storage=self._service_data_storage,
                           reinforcement_algorithm_array=self._reinforcement_algorithm_array,
                           episode_n=i,
                           best=self._best,
                           round_time_s=self._round_time_s)

            sim.init()
            sim.simulate()

            Log.minfo(ExperimentEpisodes._MODULE, f"run: End episode {i}")

        Log.minfo(ExperimentEpisodes._MODULE, f"run: Experiment finished")


def main(argv):
    n_nodes = 3
    round_time_s = 30

    session_id = Utils.current_time_string()
    data_storage = ServiceDataStorage(session_id)
    best = Best()

    rl_algorithm_array = [ActorCritic(n_actions=n_nodes) for _ in range(n_nodes)]

    exp = ExperimentEpisodes(service_data_storage=data_storage,
                             reinforcement_algorithm_array=rl_algorithm_array,
                             max_episodes=int(1e8),
                             best=best,
                             round_time_s=round_time_s)
    exp.run()

    data_storage.done_experiment()


if __name__ == "__main__":
    main(sys.argv)
