#  Copyright (c) 2023. Gabriele Proietti Mattia <pm.gabriele@gmail.com>
#  simulator-2023-cloudcom - Simpy simulator of energy schedulers
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.

import traceback

import torch

from best import Best
from log import Log
from reinforcement import ReinforcementAlgorithm
from service_data_storage import ServiceDataStorage
from service_discovery import ServiceDiscovery
from utils import Utils


class Orchestrator:
    """The orchestrator knows the state of every node and updates the migration ratio"""

    _MODULE = "Orchestrator"
    _DEBUG = False

    def __init__(self, env, round_time=30, episode_n=0, reinforcement_algorithm: ReinforcementAlgorithm = None,
                 best: Best = None):
        self._env = env  # type: simpy.Environment
        """Simpy environment"""
        self._round_time = round_time
        """Duration of each round"""
        self._service_discovery = None  # type: ServiceDiscovery or None
        """The Discovery service"""
        self._service_data_storage = None  # type: ServiceDataStorage or None
        """The Data Storage service"""
        self._episode_n = episode_n
        """Th number of the episode"""

        self._best = best

        self._d_round_batteries_residual = {}
        self._d_round_batteries_residual_perc = {}
        self._d_round_rejected_rate = {}
        self._d_round_forwarded_rate = {}
        self._d_round_lifespan = {}
        self._d_round_requests_rate = {}
        self._d_round_requests_rate_effective = {}
        self._d_round_batteries_discharge = {}
        self._d_round_operations_time = {}
        self._d_round_operations_discharge = {}
        self._l_round_accepted_tasks = []

        self._reinforcement_algorithm = reinforcement_algorithm
        self._device = Utils.hardware_check()

        # processes
        self._process_round = self._env.process(self._process_round())
        """Process for round"""
        self._round_number = 0

        Log.minfo(f"{Orchestrator._MODULE}", f"__init__: done, round_time={self._round_time}")

        self._nodes_n = 0
        self._last_migration_m = [[0.0 for _ in range(self._nodes_n)] for _ in range(self._nodes_n)]
        self._last_state_m = []
        self._last_lifespan_arr = []

        self._init_module = False

        if self._best.get_best(Best.Value.LIFESPAN) is None:
            self._best.set_best(Best.Value.LIFESPAN, 0)

        self._max_diff_ls = 0.0
        self._max_exch_tasks = 0.0

    def _process_round(self):
        stop_process = False

        while True:
            try:
                yield self._env.timeout(self._round_time)

                if Orchestrator._DEBUG:
                    Log.mdebug(Orchestrator._MODULE, f"_process_round: started round {self._round_number}")

                # skip learning on first round
                if self._round_number == 0:
                    for i, node in enumerate(self._service_discovery.get_all_nodes()):
                        node.a_round(1.0, None)
                    self._round_number += 1
                    continue

                stop_process = self._collect_metrics()

                # log things
                self._service_data_storage.log_matrix_round(self._round_number, self._episode_n, self._last_migration_m,
                                                            matrix_type="migration")
                self._service_data_storage.log_metrics_all_nodes_round(episode_n=self._episode_n,
                                                                       round_n=self._round_number,
                                                                       batteries=self._d_round_batteries_residual,
                                                                       rejected_perc_dict=self._d_round_rejected_rate,
                                                                       forwarded_perc_dict=self._d_round_forwarded_rate,
                                                                       arrival_rates_dict=self._d_round_requests_rate,
                                                                       requests_rate_effective=self._d_round_requests_rate_effective,
                                                                       lifespan_dict=self._d_round_lifespan,
                                                                       battery_discharge_dict=self._d_round_batteries_discharge,
                                                                       operations_time=self._d_round_operations_time,
                                                                       operations_discharge=self._d_round_operations_discharge,
                                                                       )

                state_matrix = self._prepare_state()
                # Log.mdebug(f"{Orchestrator._MODULE}", f"self._last_state_m={self._last_state_m} state_matrix={state_matrix}")

                # ask new ratios to dnn
                policy_t, _ = self._reinforcement_algorithm.predict(state_matrix)
                new_migration_matrix = Utils.convert_policy_to_migration_ratios(self._nodes_n, policy_t)

                # calculate reward
                reward_matrix = self._compute_reward_matrix(episode_end=stop_process)
                self._service_data_storage.log_matrix_round(self._round_number, self._episode_n, reward_matrix,
                                                            matrix_type="reward")
                # log.Log.mdebug(Orchestrator._MODULE, f"_process_round: reward_matrix={reward_matrix}")

                # train the model
                loss_actor, loss_critic = self._reinforcement_algorithm.train(self._last_state_m, state_matrix,
                                                                              reward_matrix,
                                                                              done_episode=stop_process)

                # episode is ended
                if stop_process:
                    self._episode_end(loss_actor=loss_actor, loss_critic=loss_critic)
                    self._stop_everything()
                    break

                # update the last state
                self._last_state_m = state_matrix
                self._last_migration_m = new_migration_matrix

                # set new ratios
                for i, node in enumerate(self._service_discovery.get_all_nodes_sorted()):
                    to_self = 0.0
                    to_neighbours = []

                    for j in range(len(new_migration_matrix[i])):
                        if j == i:
                            to_self = new_migration_matrix[i][j]
                        else:
                            to_neighbours.append(new_migration_matrix[i][j])

                    if Orchestrator._DEBUG:
                        Log.mdebug(f"{Orchestrator._MODULE}",
                                   f"_process_round: to_self={to_self}, to_neighbours={to_neighbours}")
                    node.a_round(to_self, to_neighbours)
                    # node.a_round(1.0, [0.0 for _ in range(self._nodes_n - 1)])

                if self._round_number % 100 == 0:
                    Log.minfo(f"{Orchestrator._MODULE}",
                              f"_process_round: ended round {self._round_number} of episode {self._episode_n}: loss_actor={loss_actor} loss_critic={loss_critic}")

                self._round_number += 1

            except Exception as e:
                traceback.print_tb()
                Log.warn(f"{Orchestrator._MODULE}", f"_process_round: job executor interrupted, e={e}")
                break

        Log.warn(f"{Orchestrator._MODULE}", f"_process_round: interrupted")

    #
    # Utils
    #

    def _collect_metrics(self):
        """Collect all the round metrics from all nodes"""
        stop_process = False

        self._l_round_accepted_tasks = []
        # save past info
        for node in self._service_discovery.get_all_nodes_sorted():
            self._d_round_batteries_residual[str(node.get_uid())] = node.get_battery_residual()
            self._d_round_batteries_residual_perc[str(node.get_uid())] = node.get_battery_residual_percentage()
            self._d_round_rejected_rate[str(node.get_uid())] = node.get_perc_round_rejected()
            self._d_round_forwarded_rate[str(node.get_uid())] = node.get_perc_round_forwarded()
            self._d_round_lifespan[str(node.get_uid())] = node.get_lifespan_round(self._round_time)
            self._d_round_requests_rate[str(node.get_uid())] = node.get_requests_rate(self._round_time)
            self._d_round_requests_rate_effective[str(node.get_uid())] = node.get_requests_rate_effective(
                self._round_time)
            self._d_round_batteries_discharge[str(node.get_uid())] = node.get_battery_discharge_round(self._round_time)
            self._l_round_accepted_tasks.append(node.get_round_list_accepted_tasks())

            operations_times = node.get_operations_time_round()
            for i, time in enumerate(operations_times):
                self._d_round_operations_time[f"{node.get_uid()}-{i}"] = time

            operations_discharge = node.get_operations_battery_discharge_round()
            for i, discharge in enumerate(operations_discharge):
                self._d_round_operations_discharge[f"{node.get_uid()}-{i}"] = discharge

            # Log.mdebug(f"{Orchestrator._MODULE}",
            #            f"_collect_metrics: ep={self._episode_n} avg_ls={sum(self._d_round_lifespan.values()) / len(self._d_round_lifespan.values())}")

            # stop the simulation when at least one node's battery finishes
            if node.get_battery_residual() <= 0.0:
                stop_process = True

            # log round end
            self._service_data_storage.done_round_for_node(self._round_number,
                                                           self._episode_n,
                                                           node.get_uid(),
                                                           node.get_battery_residual())

        return stop_process

    def _prepare_state(self) -> (torch.Tensor, list[list[float]]):
        input_matrix = [
            [],  # batteries
            [],  # rate
        ]

        # batteries
        for node_uid in self._service_discovery.get_all_nodes_id_sorted():
            input_matrix[0].append(self._d_round_batteries_residual_perc[str(node_uid)])  # 1.0
            input_matrix[1].append(self._d_round_requests_rate[str(node_uid)])  # 1.0

        # input_tensor = torch.Tensor(input_matrix)
        # input_tensor.requires_grad = True

        return input_matrix

    def _compute_reward_matrix(self, episode_end=False):
        reward_matrix = []
        all_nodes_uids = self._service_discovery.get_all_nodes_id_sorted()

        all_lifespans = []
        for i, node_uid in enumerate(all_nodes_uids):
            all_lifespans.append(self._d_round_lifespan[str(node_uid)])

        # ls at t+1
        min_lifespan_t1 = min(all_lifespans)
        max_lifespan_t1 = max(all_lifespans)
        avg_lifespan_t1 = sum(all_lifespans) / len(all_lifespans)
        rng_lifespan_t1 = max_lifespan_t1 - min_lifespan_t1

        # ls at t
        # min_lifespan_t0 = min(self._last_lifespan_arr)
        # max_lifespan_t0 = max(self._last_lifespan_arr)
        # rng_lifespan_t0 = max_lifespan_t0 - min_lifespan_t0

        # useful fns
        def reward_fn_avg_lifespan(node_i, lifespan_diff_vec, max_diff=0.0, max_reward=200):
            if max_diff < 1e-2:
                return max_reward

            # if min(lifespan_diff_vec) < 0:
            # m, q = Utils.line_by_two_points(-max_diff, 0, -max_reward, 0)
            # r = m * min(lifespan_diff_vec) + q
            #    return 0

            avg_hrs = sum(lifespan_diff_vec) / len(lifespan_diff_vec)
            diff_from_avg = all_lifespans[node_i] - avg_lifespan_t1
            if avg_hrs < 0:
                m, q = Utils.line_by_two_points(-max_diff, 0, -max_reward, 0)
                r = m * avg_hrs + q
                return r

            m, q = Utils.line_by_two_points(0, max_diff, max_reward, 0)
            r = m * avg_hrs + q

            return r

        def reward_fn_diff_lifespan(diff_hrs, max_diff=0.0, max_reward=200):
            if max_diff < 1e-2:
                return max_reward

            # if diff_hrs > 0:
            # m, q = Utils.line_by_two_points(-max_diff, 0, -max_reward, 0)
            # r = m * diff_hrs + q
            #    return 0

            m, q = Utils.line_by_two_points(0, max_diff, max_reward, 0)
            r = m * abs(diff_hrs) + q

            return r

        def reward_fn_exchanged_tasks(diff_tasks, i=None, j=None, max_tasks=0.0, max_reward=200):
            if max_tasks < 1e-2:
                return max_reward

            m, q = Utils.line_by_two_points(0, max_tasks, max_reward, 0)
            return m * diff_tasks + q

        def get_exchg_tasks(i, j):
            t_ij = self._last_migration_m[i][j] * self._last_state_m[1][i]
            t_ji = self._last_migration_m[j][i] * self._last_state_m[1][j]
            return t_ij + t_ji

        # sum of differences
        diff_vec = []
        diff_vec_abs = []
        exch_tasks = []
        for i, node_i_uid in enumerate(all_nodes_uids):
            total_diff = 0.0
            total_diff_abs = 0.0
            total_exch_tasks = 0.0
            for j, node_j_uid in enumerate(all_nodes_uids):
                total_diff += all_lifespans[i] - all_lifespans[j]
                total_diff_abs += abs(all_lifespans[i] - all_lifespans[j])
                total_exch_tasks += get_exchg_tasks(i, j)
            diff_vec.append(total_diff)
            diff_vec_abs.append(total_diff_abs)
            exch_tasks.append(total_exch_tasks)
        max_diff_ls = max(diff_vec_abs)
        max_exch_tasks = max(exch_tasks)

        if avg_lifespan_t1 > self._max_diff_ls:
            self._max_diff_ls = avg_lifespan_t1
        if max_exch_tasks > self._max_exch_tasks:
            self._max_exch_tasks = max_exch_tasks

        for i, node_i_uid in enumerate(all_nodes_uids):
            reward_matrix.append([])
            for j, node_j_uid in enumerate(all_nodes_uids):
                reward = 0
                # Log.mdebug(Orchestrator._MODULE, f"self._l_round_accepted_tasks={self._l_round_accepted_tasks}")

                i_in_range = avg_lifespan_t1 * .95 < all_lifespans[i] < avg_lifespan_t1 * 1.05
                i_above_range = all_lifespans[i] > avg_lifespan_t1 * 1.05
                i_below_range = all_lifespans[i] < avg_lifespan_t1 * .95

                if j == i:
                    if i_in_range:
                        reward = 0
                    elif i_above_range:
                        reward = -1
                    elif i_below_range:
                        reward = 1
                    else:
                        Log.mfatal(Orchestrator._MODULE, "case not addressed")

                    # reward = self._l_round_accepted_tasks[i][0]
                    # lifespan_diff_t0 = self._last_lifespan_arr[i]
                    # lifespan_diff_t1 = all_lifespans[i]
                    # (lifespan_diff_t1 - lifespan_diff_t0) + end_episode_reward)
                    # reward = (sum(diff_vec_abs) / len(diff_vec_abs))  # - (sum(exch_tasks) / len(exch_tasks))
                    # reward += 1 * reward_fn_diff_lifespan(-1 * (all_lifespans[i] - avg_lifespan_t1), max_diff=self._max_diff_ls)
                    # reward +=  reward_fn_diff_lifespan(abs(all_lifespans[i] - avg_lifespan_t1), max_diff=self._max_diff_ls)
                    # reward += - abs(all_lifespans[i] - avg_lifespan_t1)
                    # reward += .0 * reward_fn_exchanged_tasks(sum(exch_tasks) / len(exch_tasks), max_tasks=self._max_exch_tasks)
                    # reward = 0
                    # reward += reward_fn_avg_lifespan(diff_vec, max_diff=self._max_diff_ls)
                    # print(f"reward = {reward}")
                else:
                    # reward = self._l_round_accepted_tasks[i][j if i < j else j + 1]
                    # penalty for exchanged traffic
                    # perc_not_lost = (
                    #         (self._last_migration_m[i][j] - self._last_migration_m[j][i]) / self._last_migration_m[i][j]) if \
                    #     self._last_migration_m[i][j] > 0 else 0.0

                    # lifespan_diff_t0 = self._last_lifespan_arr[i] - self._last_lifespan_arr[j]
                    # lifespan_diff_t1 = abs(all_lifespans[i] - all_lifespans[j])
                    # lifespan_sum_t1 = abs(all_lifespans[i] + all_lifespans[j])
                    # reward_matrix[i].append(-1 * (lifespan_diff_t1 - lifespan_diff_t0) + end_episode_reward)
                    # reward = (1 - (lifespan_diff_t1 / lifespan_sum_t1)) * 100  # + perc_not_lost * 100
                    # reward = diff_reward_fn(lifespan_diff_t1)  # + perc_not_lost * 100
                    # delta_avg_node_i = avg_lifespan_t1
                    # reward = - lifespan_diff_t1  # - get_exchg_tasks(i, j)
                    # reward += 1 * reward_fn_diff_lifespan(lifespan_diff_t1, max_diff=self._max_diff_ls)
                    # avg_ls = .5 * (abs(all_lifespans[i] - avg_lifespan_t1) + abs(all_lifespans[j] - avg_lifespan_t1))
                    # reward += - avg_ls
                    # reward +=  reward_fn_diff_lifespan(avg_ls, max_diff=self._max_diff_ls)
                    # reward += .0 * reward_fn_exchanged_tasks(get_exchg_tasks(i, j), i=i, j=j, max_tasks=self._max_exch_tasks)
                    # reward = 10
                    # reward += reward_fn_diff_lifespan(all_lifespans[i] - all_lifespans[j], max_diff=self._max_diff_ls)

                    j_in_range = avg_lifespan_t1 * .95 < all_lifespans[j] < avg_lifespan_t1 * 1.05
                    j_above_range = all_lifespans[j] > avg_lifespan_t1 * 1.05
                    j_below_range = all_lifespans[j] < avg_lifespan_t1 * .95

                    # i above
                    if i_above_range and j_above_range:
                        reward = -1
                    elif i_above_range and j_in_range:
                        reward = -1
                    elif i_above_range and j_below_range:
                        reward = -1
                    # i below
                    elif i_below_range and j_above_range:
                        reward = 1
                    elif i_below_range and j_in_range:
                        reward = -1
                    elif i_below_range and j_below_range:
                        reward = -1
                    # i in range
                    elif i_in_range and j_above_range:
                        reward = 1
                    elif i_in_range and j_in_range:
                        reward = 0
                    elif i_in_range and j_below_range:
                        reward = -1
                    else:
                        Log.mfatal(Orchestrator._MODULE, f"case not addressed: i_in_range={i_in_range} "
                                                         f"j_in_range={j_in_range} "
                                                         f"i_above_range={i_above_range} "
                                                         f"j_above_range={j_above_range} "
                                                         f"i_below_range={i_below_range} "
                                                         f"j_below_range={j_below_range}"
                                   )

                # lifespan_diff_t1_t0 = lifespan_diff_t1 - lifespan_diff_t0
                # lifespan_diff_t1_t0_perc = lifespan_diff_t1_t0 / (lifespan_diff_t1 + lifespan_diff_t0)
                # if lifespan_diff_t1_t0_perc < 0:
                #     reward = -1.0
                # elif lifespan_diff_t1_t0_perc > .05:
                #     reward = -1.0  # good
                # elif lifespan_diff_t1_t0_perc < .05:
                #     reward = -1.0  # bad
                # else:
                #     reward = 1.0  # neutral

                if False:
                    Log.mdebug(Orchestrator._MODULE,
                               f"i={i} j={j} reward={reward:.3f} "
                               f"m[i][j]={self._last_migration_m[i][j]:.3f} "
                               f"m[j][i]={self._last_migration_m[j][i]:.3f} "
                               # f"lifespan_diff_t1={lifespan_diff_t1:.3f} "
                               f"self._max_diff_ls={self._max_diff_ls:.3f} "
                               f"self._last_state_m[1][i]={self._last_state_m[1][i]:.3f} "
                               f"self._last_state_m[1][j]={self._last_state_m[1][j]:.3f} "
                               f"diff_vec={diff_vec} "
                               f"avg_lifespan_t1={avg_lifespan_t1} "
                               f"all_lifespans[i]={all_lifespans[i]:.3f} "
                               f"all_lifespans[j]={all_lifespans[j]:.3f} "
                               f"abs(all_lifespans[i] - avg_lifespan_t1) = {abs(all_lifespans[i] - avg_lifespan_t1)} "
                               )

                reward_matrix[i].append(reward)

        # normalize the reward matrix per row
        # for i, node_i_uid in enumerate(all_nodes_uids):
        #     row_sum = sum(reward_matrix[i])
        #     for j, node_j_uid in enumerate(all_nodes_uids):
        #         reward_matrix[i][j] = (reward_matrix[i][j] / row_sum) * 100

        # update last lifespans
        self._last_lifespan_arr = all_lifespans

        # Log.mdebug(f"{Orchestrator._MODULE}", f"lifespans={all_lifespans} reward_matrix={reward_matrix}")

        return reward_matrix

    def _episode_end(self, loss_actor, loss_critic):
        d_rejected_rate = {}
        d_forwarded_rate = {}

        for node in self._service_discovery.get_all_nodes():
            d_rejected_rate[str(node.get_uid())] = node.get_perc_total_rejected()
            d_forwarded_rate[str(node.get_uid())] = node.get_perc_total_forwarded()

        self._service_data_storage.log_metrics_episode(self._episode_n,
                                                       rejected_perc_dict=d_rejected_rate,
                                                       forwarded_perc_dict=d_forwarded_rate,
                                                       lifespan_min=self._env.now,
                                                       loss_actor=loss_actor,
                                                       loss_critic=loss_critic)

        Log.minfo(Orchestrator._MODULE,
                  f"Orchestrator._episode_end: now={self._env.now} current_best={self._best.get_best(Best.Value.LIFESPAN)}")

        # save models if best
        if self._env.now > self._best.get_best(Best.Value.LIFESPAN):
            self._best.set_best(Best.Value.LIFESPAN, self._env.now)
            self._service_data_storage.save_models(f"ep_{self._episode_n}_ls_{str(int(self._env.now))}",
                                                   self._reinforcement_algorithm)

    def _stop_everything(self):
        """Stop the processes of all nodes and the current, this ends the simulation"""
        for node in self._service_discovery.get_all_nodes():
            node.a_stop()

    #
    # Setters
    #

    def _init(self):
        self._init_module = True

        self._nodes_n = len(self._service_discovery.get_all_nodes())
        self._last_migration_m = [[0.0 for _ in range(self._nodes_n)] for _ in range(self._nodes_n)]
        self._last_lifespan_arr = [0.0 for _ in range(self._nodes_n)]

        self._collect_metrics()
        self._last_state_m = self._prepare_state()

    def set_service_discovery(self, service_discovery: ServiceDiscovery):
        self._service_discovery = service_discovery

        if self._service_data_storage is not None and not self._init_module:
            self._init_module = True
            self._init()

    def set_service_data_storage(self, service_data_storage: ServiceDataStorage):
        self._service_data_storage = service_data_storage

        if self._service_discovery is not None and not self._init_module:
            self._init_module = True
            self._init()
