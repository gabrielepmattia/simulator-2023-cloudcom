#  Copyright (c) 2023. Gabriele Proietti Mattia <pm.gabriele@gmail.com>
#  simulator-2023-cloudcom - Simpy simulator of energy schedulers
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.

import torch

from dnn import DNNManager
# from reinforcement import PolicyGradient
from utils import Utils

device = Utils.hardware_check()

N_NODES = 5
dnn_manager = DNNManager(inputs_n=N_NODES * 2, outputs_n=N_NODES * N_NODES)
dnn_manager = Utils.to_device(dnn_manager, device)

# sarsa_avg = PolicyGradient(n_nodes=5, device=device)

for i in range(200):
    print()
    input_matrix = [
        [0.7, 0.5, 0.7, 0.5, 0.5],  # batteries
        [3.0, 4.0, 2.0, 5.0, 1.0],  # rate
    ]

    input_tensor = torch.Tensor(input_matrix)
    input_tensor.requires_grad = True
    input_tensor = Utils.to_device(input_tensor, device)

    estimate_dummy = dnn_manager.forward(input_tensor)
    print(f"estimate_dummy={estimate_dummy}")

    target_vec = [
        100, 100, 100, 100, 100,
        100, 100, 100, 100, 100,
        100, 100, 100, 100, 100,
        100, 100, 100, 100, 100,
        100, 100, 100, 100, 100,
    ]

    reward_m = [
        [100, 100, 100, 100, 100],
        [100, 100, 100, 100, 100],
        [100, 100, 100, 100, 100],
        [100, 100, 100, 100, 100],
        [100, 100, 100, 100, 100],
    ]

    target = torch.Tensor(target_vec)
    target.requires_grad = True
    target_dummy = Utils.to_device(target, device)

    # estimate, target = sarsa_avg.target(reward_m, input_tensor, input_tensor, dnn_manager)

    loss = dnn_manager.learn(estimate_dummy, target_dummy)
    # loss = dnn_manager.learn(estimate, target)
    print(f"estimate_dummy={estimate_dummy}")
    print(f"target={target}")
    print(f"target_dummy={target_dummy}")
    print(f"loss={loss}")

    estimate = dnn_manager.forward(input_tensor)
    print(estimate)
