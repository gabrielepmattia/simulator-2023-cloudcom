#  Copyright (c) 2023. Gabriele Proietti Mattia <pm.gabriele@gmail.com>
#  simulator-2023-cloudcom - Simpy simulator of energy schedulers
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.

import dataclasses
from typing import List

import torch
from torch import Tensor

from dnn import DNNManager
from utils import Utils


@dataclasses.dataclass
class TrainingSample:
    state: List[float]
    action: float
    reward: float


class ReinforcementAlgorithm:

    def train(self, batch: List[TrainingSample], done_episode=False):
        pass

    def predict_t(self, state_tensor: Tensor):
        pass

    def predict(self, state_matrix):
        pass

    def save_models(self, path, filename):
        pass

    def get_id(self):
        pass


class ActorCritic(ReinforcementAlgorithm):
    _MODULE = "ActorCritic"

    def __init__(self, node_id=-1, n_actions=-1, gamma=0.9, beta=0.1):
        self._node_id = node_id
        self._n_actions = n_actions
        self._state_size = 4

        self._gamma = gamma
        self._beta = beta

        self._device = Utils.hardware_check()

        self._dnn_actor = DNNManager(inputs_n=self._state_size,
                                     outputs_n=self._n_actions,
                                     loss=DNNManager.LossToUse.ACTOR,
                                     last_activation_fn=DNNManager.LastActivationFn.SOFTMAX)
        self._dnn_critic = DNNManager(inputs_n=self._state_size,
                                      outputs_n=1,
                                      loss=DNNManager.LossToUse.HUBER)
        # move to device
        self._dnn_actor = Utils.to_device(self._dnn_actor, self._device)
        self._dnn_critic = Utils.to_device(self._dnn_critic, self._device)

        self._average_reward = [[0.0 for _ in range(self._n_actions)] for _ in range(self._n_actions)]

    def predict_t(self, state_tensor: Tensor):
        return self._predict_actor_t(state_tensor), self._predict_critic_t(state_tensor)

    def predict(self, state_matrix):
        return self._predict_actor(state_matrix), self._predict_critic(state_matrix)

    def train(self, batch: List[TrainingSample], done_episode=False):
        actor_estimate = []
        actor_target = []
        critic_estimate = []
        critic_target = []

        sum_rewards_total = sum(e.reward for e in batch)
        sum_rewards_total_up_to_i = 0.0

        for i, sample in enumerate(batch):
            # sample for critic
            value = self._predict_critic([sample.state])
            return_ = sample.reward + self._gamma * value  # (sum_rewards_total - sum_rewards_total_up_to_i)  # TODO: improve the expected reward
            advantage = return_ - value

            critic_estimate.append(value)
            critic_target.append(torch.Tensor([return_]))

            # samples for actor
            actor_estimate.append(self._predict_actor([sample.state]))
            actor_target.append(
                torch.Tensor([advantage if i == sample.action else 0.0 for i in range(self._n_actions)]))

            # update vars
            sum_rewards_total_up_to_i += sample.reward

        actor_estimate_t = Utils.to_device(torch.cat(actor_estimate), self._device)
        actor_target_t = Utils.to_device(torch.cat(actor_target), self._device)
        critic_estimate_t = Utils.to_device(torch.cat(critic_estimate), self._device)
        critic_target_t = Utils.to_device(torch.cat(critic_target), self._device)

        actor_estimate_t = torch.reshape(actor_estimate_t, (len(batch), self._n_actions))
        actor_target_t = torch.reshape(actor_target_t, (len(batch), self._n_actions))
        critic_estimate_t = torch.reshape(critic_estimate_t, (len(batch), 1))
        critic_target_t = torch.reshape(critic_target_t, (len(batch), 1))

        # Log.mdebug(ActorCritic._MODULE, f"actor_estimate_t={actor_estimate_t.shape}")
        # Log.mdebug(ActorCritic._MODULE, f"actor_target_t={actor_target_t.shape}")
        # Log.mdebug(ActorCritic._MODULE, f"critic_estimate_t={critic_estimate_t.shape}")
        # Log.mdebug(ActorCritic._MODULE, f"critic_target_t={critic_target_t.shape}")

        loss_critic = self._dnn_critic.learn(critic_estimate_t, critic_target_t)
        loss_actor = self._dnn_critic.learn(actor_estimate_t, actor_target_t)

        """
        prev_state_t = Utils.to_device(Utils.to_tensor(prev_state), self._device)
        next_state_t = Utils.to_device(Utils.to_tensor(next_state), self._device)
        # reward_matrix_t = Utils.to_tensor(reward_matrix)

        # values
        qsa_prev_state = self._predict_critic_t(prev_state_t)
        qsa_next_state = self._predict_critic_t(next_state_t)
        qsa_prev_state_l = qsa_prev_state.tolist()
        qsa_next_state_l = qsa_next_state.tolist()
        policy_prev_state = self._predict_actor_t(prev_state_t)
        policy_next_state = self._predict_actor_t(next_state_t)

        # compute value of next state for every row
        v_next_state = max(qsa_next_state_l)
        v_prev_state = max(qsa_prev_state_l)

        # compute migration matrix
        # m_prev = Utils.convert_policy_to_migration_ratios(self._n_nodes, policy_prev_state)

        # compute critic and actor targets
        td_target_list = []
        td_error_list = []
        for i in range(self._n_nodes):
            target = reward_list[i] + (self._gamma * v_next_state) if not done_episode else 0.0
            # target = reward_matrix[i][j] + self._gamma * qsa_next_state_l[i * self._n_nodes + j]
            # self._average_reward[i][j] += self._beta * target

            td_target_list.append(target)
            td_error_list.append(target - v_prev_state)  # qsa_prev_state_l[i * self._n_nodes + j])

        td_target_t = Utils.to_device(Utils.to_tensor(td_target_list), self._device)
        td_error_t = Utils.to_device(Utils.to_tensor(td_error_list), self._device)

        torch.autograd.set_detect_anomaly(True)

        # train critic
        loss_critic = self._dnn_critic.learn(qsa_prev_state, td_target_t)

        # train actor
        loss_actor = self._dnn_actor.learn(policy_prev_state, td_error_t)

        # Log.mdebug(ActorCritic._MODULE, f"qsa_prev_state_l={qsa_prev_state_l}")
        # Log.mdebug(ActorCritic._MODULE, f"policy_prev_state={policy_prev_state.tolist()}")
        """

        return loss_actor, loss_critic

    def save_models(self, path, filename):
        self._dnn_actor.save(path, f"{filename}_actor")
        self._dnn_critic.save(path, f"{filename}_critic")

    def get_id(self):
        return self._node_id

    #
    # Utils
    #

    def _predict_actor(self, state_matrix: list[list]) -> Tensor:
        state_matrix_t = Utils.to_device(Utils.to_tensor(state_matrix), self._device)
        return self._dnn_actor.forward(state_matrix_t)

    def _predict_critic(self, state_matrix: list[list]) -> Tensor:
        state_matrix_t = Utils.to_device(Utils.to_tensor(state_matrix), self._device)
        return self._dnn_critic.forward(state_matrix_t)

    def _predict_actor_t(self, state_tensor: Tensor) -> Tensor:
        return self._dnn_actor.forward(state_tensor)

    def _predict_critic_t(self, state_tensor: Tensor) -> Tensor:
        return self._dnn_critic.forward(state_tensor)
