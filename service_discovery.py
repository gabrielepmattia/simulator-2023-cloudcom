#  Copyright (c) 2023. Gabriele Proietti Mattia <pm.gabriele@gmail.com>
#  simulator-2023-cloudcom - Simpy simulator of energy schedulers
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.

from __future__ import annotations

from typing import List, Dict

# from node import Node
from log import Log
from node import Node

"""
Implementation of the discovery service for nodes
"""

DEBUG = False


class ServiceDiscovery:
    """Module for allowing nodes cooperation"""
    _MODULE = "ServiceDiscovery"

    def __init__(self, nodes: List[Node], topology: Dict):
        self._nodes = nodes  # type: 'List[Node]'
        self._n_nodes = len(nodes)
        self._topology = topology  # dict[node_uid] = [uid, uid, ...]
        # sort topology
        for node_uid in self._topology.keys():
            self._topology[node_uid] = sorted(self._topology[node_uid])

        self._nodes_by_uid = {}
        for node in self._nodes:
            self._nodes_by_uid[node.get_uid()] = node

        # sorted list of nodes
        all_nodes_uids = sorted(self._topology.keys())
        self._nodes_sorted_by_uid = [self._nodes_by_uid[uid] for uid in all_nodes_uids]

        Log.minfo(ServiceDiscovery._MODULE, f"Initialized {self._n_nodes} nodes")

        self._max_episode_logged = 0
        self._max_round_logged = 0

        # stop request
        self._stopped_requests = 0
        """Number of nodes which triggered end episode"""

    def get_node_by_uid(self, node_uid) -> Node:
        return self._nodes_by_uid[node_uid]

    def get_neighbours(self, current_node_uid=0) -> List[Node]:
        neighbours = []
        for node_uid in self._topology[current_node_uid]:
            neighbours.append(self._nodes_by_uid[node_uid])
        return neighbours

    def get_all_nodes(self):
        return self._nodes

    def get_all_nodes_id_sorted(self):
        return sorted(self._topology.keys())

    def get_all_nodes_sorted(self) -> List[Node]:
        return self._nodes_sorted_by_uid

    def get_configuration_dict(self):
        conf_dict = {
            "nodes": [],
            "topology": self._topology,
        }
        for node in self.get_all_nodes():
            conf_dict["nodes"].append(node.get_configuration_dict())

        return conf_dict

    def end_episode(self, episode_n=-1, service_data_storage=None):
        """
        Triggers the end of the episode by stopping all the nodes
        """

        # self._stopped_requests += 1
        # if self._stopped_requests != len(self.get_all_nodes()):
        #     return

        Log.minfo(ServiceDiscovery._MODULE, "end_episode: declared episode end")

        # for node in self.get_all_nodes():
        #     node.a_stop(originator=originator_node_uid == node.get_uid())

        d_rejected_rate = {}
        d_forwarded_rate = {}

        for node in self.get_all_nodes():
            d_rejected_rate[str(node.get_uid())] = node.get_perc_total_rejected()
            d_forwarded_rate[str(node.get_uid())] = node.get_perc_total_forwarded()

        service_data_storage.log_metrics_episode(episode_n,
                                                 rejected_perc_dict=d_rejected_rate,
                                                 forwarded_perc_dict=d_forwarded_rate,
                                                 # lifespan_min=lifespan_min,
                                                 # loss_actor=0.0,  # loss_actor,
                                                 # loss_critic=0.0  # loss_critic
                                                 )

    def log_round(self, originator_node_uid=-1, episode_n=-1, round_n=-1, service_data_storage=None, round_time=30):
        if episode_n <= self._max_episode_logged and round_n <= self._max_round_logged:
            return

        self._max_round_logged = round_n
        self._max_episode_logged = episode_n

        self._collect_metrics(episode_n, round_n, service_data_storage, round_time)

    def _collect_metrics(self, episode, round_n, service_data_storage=None, round_time=30):
        d_round_batteries_residual = {}
        d_round_batteries_residual_perc = {}
        d_round_rejected_rate = {}
        d_round_forwarded_rate = {}
        d_round_lifespan = {}
        d_round_requests_rate = {}
        d_round_requests_rate_effective = {}
        d_round_batteries_discharge = {}
        d_round_operations_time = {}
        d_round_operations_discharge = {}
        d_round_energy_consumption = {}
        l_round_accepted_tasks = []

        l_round_accepted_tasks = []
        # save past info
        for node in self.get_all_nodes_sorted():
            d_round_batteries_residual[str(node.get_uid())] = node.get_battery_residual()
            d_round_batteries_residual_perc[str(node.get_uid())] = node.get_battery_residual_percentage()
            d_round_rejected_rate[str(node.get_uid())] = node.get_perc_round_rejected()
            d_round_forwarded_rate[str(node.get_uid())] = node.get_perc_round_forwarded()
            d_round_lifespan[str(node.get_uid())] = node.get_lifespan_last_round()
            d_round_requests_rate[str(node.get_uid())] = node.get_requests_rate()
            d_round_requests_rate_effective[str(node.get_uid())] = node.get_requests_rate_effective()
            d_round_batteries_discharge[str(node.get_uid())] = node.get_battery_discharge_round()
            d_round_energy_consumption[str(node.get_uid())] = node.get_power_last_round_w()
            l_round_accepted_tasks.append(node.get_round_list_accepted_tasks())

            operations_times = node.get_operations_time_round()
            for i, time in enumerate(operations_times):
                d_round_operations_time[f"{node.get_uid()}-{i}"] = time

            operations_discharge = node.get_operations_battery_discharge_round()
            for i, discharge in enumerate(operations_discharge):
                d_round_operations_discharge[f"{node.get_uid()}-{i}"] = discharge

        migration_matrix = []
        for i, nodeI in enumerate(self.get_all_nodes_sorted()):
            migration_matrix.append([])
            ratio_local, ratios_neighbours = nodeI.get_migration_ratios()
            for j, nodeJ in enumerate(self.get_all_nodes_sorted()):
                if i == j:
                    migration_matrix[i].append(ratio_local)
                elif j < i:
                    migration_matrix[i].append(ratios_neighbours[j])
                else:
                    migration_matrix[i].append(ratios_neighbours[j - 1])

        service_data_storage.log_matrix_round(round_n, episode, migration_matrix,
                                              matrix_type="migration")

        service_data_storage.log_metrics_all_nodes_round(episode_n=episode,
                                                         round_n=round_n,
                                                         batteries_residual_perc=d_round_batteries_residual_perc,
                                                         rejected_perc_dict=d_round_rejected_rate,
                                                         forwarded_perc_dict=d_round_forwarded_rate,
                                                         arrival_rates_dict=d_round_requests_rate,
                                                         requests_rate_effective=d_round_requests_rate_effective,
                                                         lifespan_dict=d_round_lifespan,
                                                         battery_discharge_dict=d_round_batteries_discharge,
                                                         operations_time=d_round_operations_time,
                                                         operations_discharge=d_round_operations_discharge,
                                                         energy_consumption_dict=d_round_energy_consumption,
                                                         )
