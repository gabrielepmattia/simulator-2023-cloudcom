#  Copyright (c) 2023. Gabriele Proietti Mattia <pm.gabriele@gmail.com>
#  simulator-2023-cloudcom - Simpy simulator of energy schedulers
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.

from typing import List

from best import Best
from log import Log
from models import NodeSpec, SolarTraceSpec
from node import Node
from reinforcement import ActorCritic
from service_data_storage import ServiceDataStorage
from simulate import Simulate
from utils import Utils


class ExperimentEpisodes:
    _MODULE = "ExperimentEpisodes"

    def __init__(self, service_data_storage: ServiceDataStorage, simulation_time=10000, max_episodes=1,
                 best: Best = None, round_time_s=30,
                 stop_when_node_shutdown=False,
                 # nodes and scheduler
                 scheduler_type=Node.SchedulerType.FIXED_POLICY,
                 scheduler_algorithm=Node.SchedulerFixedPolicyAlgorithm.RATIO_ADAPTIVE_STEP,
                 nodes_spec: List[NodeSpec] = (),
                 # rl
                 reinforcement_algorithm_array=None,
                 # solar solar_traces
                 solar_traces_enabled=False,
                 solar_traces_csv_list=(),
                 solar_traces_spec=(),
                 solar_traces_csv_col=1,
                 solar_traces_to_watt_multiplier=1000
                 ):
        self._max_episodes = int(max_episodes)
        """Max number of episodes"""
        self._service_data_storage = service_data_storage
        """Data storage service for storing results"""
        self._best = best
        """Best object for keeping the best results in memory"""
        self._round_time_s = round_time_s
        """Round time when nodes updates the migration ratios"""
        self._stop_when_node_shutdown = stop_when_node_shutdown
        """Stop simulation when a node shutdown"""
        self._simulation_time = simulation_time
        """Total simulation time"""

        self._reinforcement_algorithm_array = reinforcement_algorithm_array
        """Array of RL algorithm, one for each node"""

        self._scheduler_type = scheduler_type
        """Type of scheduler algorithm used by all nodes"""
        self._scheduler_algorithm = scheduler_algorithm
        """Scheduling algorithm used by all nodes"""

        self._nodes_spec = nodes_spec
        """Specification of nodes structure"""

        self._solar_traces_enabled = solar_traces_enabled
        """If solar solar_traces should be used"""
        self._solar_traces_csv_list = solar_traces_csv_list
        """List of csv with solar solar_traces"""
        self._solar_traces_spec = solar_traces_spec
        """Mapping of solar solar_traces to nodes"""
        self._solar_traces_csv_col = solar_traces_csv_col
        """Column in the csv where solar power is described"""
        self._solar_traces_to_watt_multiplier = solar_traces_to_watt_multiplier

    def run(self):
        for i in range(self._max_episodes):
            sim = Simulate(simulation_time=self._simulation_time,
                           service_data_storage=self._service_data_storage,
                           episode_n=i,
                           best=self._best,
                           round_time_s=self._round_time_s,
                           stop_when_node_shutdown=self._stop_when_node_shutdown,
                           # nodes and scheduler
                           nodes_spec=self._nodes_spec,
                           scheduler_type=self._scheduler_type,
                           scheduler_fixed_policy_algorithm=self._scheduler_algorithm,
                           # rl
                           reinforcement_algorithm_array=self._reinforcement_algorithm_array,
                           # solar solar_traces
                           solar_traces_enabled=self._solar_traces_enabled,
                           solar_traces_csv_list=self._solar_traces_csv_list,
                           solar_traces_spec=self._solar_traces_spec,
                           solar_traces_csv_col=self._solar_traces_csv_col,
                           solar_traces_to_watt_multiplier=self._solar_traces_to_watt_multiplier
                           )

            sim.init()
            sim.simulate()

            # mark end of episode
            sim.get_discovery_service().end_episode(episode_n=i,
                                                    service_data_storage=self._service_data_storage)

            Log.minfo(ExperimentEpisodes._MODULE, f"run: End episode {i}")

        Log.minfo(ExperimentEpisodes._MODULE, f"run: Experiment finished")


class RunExperiment:

    @staticmethod
    def main(scheduler_algorithm: Node.SchedulerFixedPolicyAlgorithm):
        session_id = Utils.current_time_string()
        data_storage = ServiceDataStorage(session_id, skip_db=False)
        stop_when_node_shutdown = False
        simulation_time = 2 * 24 * 3600
        solar_csv_col = 1

        nodes = [
            NodeSpec(id=0, l=7.0, m=12.0, neighbors=[1, 2, 3], battery_capacity_wh=44.44, battery_fill=0.0),
            NodeSpec(id=1, l=8.0, m=12.0, neighbors=[0, 2, 3], battery_capacity_wh=44.44, battery_fill=0.0),
            NodeSpec(id=2, l=9.0, m=12.0, neighbors=[0, 1, 3], battery_capacity_wh=44.44, battery_fill=0.0),
            NodeSpec(id=3, l=10.0, m=12.0, neighbors=[0, 1, 2], battery_capacity_wh=44.44, battery_fill=0.0),
        ]

        traces_dir = "solar_traces"

        solar_traces = [
            f"{traces_dir}/day2.csv",
            # f"{traces_dir}/data1.csv",
            # f"{traces_dir}/data2.csv"
        ]

        solar_cycles = 2
        solar_shift = 3e4

        solar_mapping = [
            SolarTraceSpec(trace_id=0, node_id=0,
                           scale_x=True, scale_x_min=0, scale_x_max=simulation_time,
                           scale_y=True, scale_y_min=0, scale_y_max=0.025, efficiency=.93, cycles=solar_cycles, shift=solar_shift),
            SolarTraceSpec(trace_id=0, node_id=1,
                           scale_x=True, scale_x_min=0, scale_x_max=simulation_time,
                           scale_y=True, scale_y_min=0, scale_y_max=0.025, efficiency=.87, cycles=solar_cycles, shift=solar_shift),
            SolarTraceSpec(trace_id=0, node_id=2,
                           scale_x=True, scale_x_min=0, scale_x_max=simulation_time,
                           scale_y=True, scale_y_min=0, scale_y_max=0.025, efficiency=.85, cycles=solar_cycles, shift=solar_shift),
            SolarTraceSpec(trace_id=0, node_id=3,
                           scale_x=True, scale_x_min=0, scale_x_max=simulation_time,
                           scale_y=True, scale_y_min=0, scale_y_max=0.025, efficiency=.8, cycles=solar_cycles, shift=solar_shift),
        ]

        # pre-set
        rl_algorithm_array = [ActorCritic(n_actions=len(nodes)) for _ in range(len(nodes))]
        best = Best()

        exp = ExperimentEpisodes(
            simulation_time=simulation_time,
            service_data_storage=data_storage,
            max_episodes=1,
            best=best,
            round_time_s=10,
            stop_when_node_shutdown=stop_when_node_shutdown,
            # nodes and scheduler
            nodes_spec=nodes,
            scheduler_type=Node.SchedulerType.FIXED_POLICY,
            scheduler_algorithm=scheduler_algorithm,  # Node.SchedulerFixedPolicyAlgorithm.RATIO_ADAPTIVE_STEP,
            # rl
            reinforcement_algorithm_array=rl_algorithm_array,
            # solar solar_traces
            solar_traces_enabled=True,
            solar_traces_csv_list=solar_traces,
            solar_traces_spec=solar_mapping,
            solar_traces_csv_col=solar_csv_col,
            solar_traces_to_watt_multiplier=1000,
        )
        exp.run()

        data_storage.done_experiment()

        return data_storage.get_session_folder()


# import cProfile
if __name__ == "__main__":
    # cProfile.run("RunExperiment.main(Node.SchedulerFixedPolicyAlgorithm.RATIO_ADAPTIVE_STEP)")
    RunExperiment.main(Node.SchedulerFixedPolicyAlgorithm.RATIO_ADAPTIVE_STEP)
