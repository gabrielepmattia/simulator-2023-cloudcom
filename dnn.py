#  Copyright (c) 2023. Gabriele Proietti Mattia <pm.gabriele@gmail.com>
#  simulator-2023-cloudcom - Simpy simulator of energy schedulers
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
from enum import Enum

import torch
import torch.nn as nn
from torch import Tensor

from log import Log


class DNNManager(nn.Module):
    _MODULE = "DNNManager"
    _DEBUG = False

    class LastActivationFn(Enum):
        NONE = 0
        SOFTMAX = 1

    class DNNToUse(Enum):
        DNN_TARGET = 0
        DNN_ONLINE = 1

    class LossToUse(Enum):
        MSE = 0
        HUBER = 1
        L1 = 2
        ACTOR = 3

    def __init__(self, inputs_n, outputs_n, loss: LossToUse = LossToUse.MSE,
                 last_activation_fn: LastActivationFn = LastActivationFn.NONE):
        super(DNNManager, self).__init__()
        self._online = DNNFC1(inputs_n, outputs_n, last_activation_fn)
        # self._target = copy.deepcopy(self._online)

        self._online.train()
        # self._target.eval()

        self.optimizer = torch.optim.Adam(self._online.parameters(), lr=1e-3)

        self.loss_fn = torch.nn.MSELoss()
        if loss == DNNManager.LossToUse.HUBER:
            self.loss_fn = torch.nn.HuberLoss(reduction='mean', delta=1.0)
        elif loss == DNNManager.LossToUse.L1:
            self.loss_fn = torch.nn.SmoothL1Loss(beta=1.0)
        elif loss == DNNManager.LossToUse.ACTOR:
            self.loss_fn = ActorLoss()

        self._last_activation_fn = last_activation_fn

    def forward(self, input_matrix, model: DNNToUse = DNNToUse.DNN_ONLINE):
        # if model == DNNManager.DNNToUse.DNN_ONLINE:
        return self._online(input_matrix)
        # elif model == DNNManager.DNNToUse.DNN_TARGET:
        # return self._target(input_matrix)

    def learn(self, estimate: Tensor, target: Tensor):
        if DNNManager._DEBUG:
            Log.mdebug(f"{DNNManager._MODULE}", f"learn: estimate={estimate}")
            Log.mdebug(f"{DNNManager._MODULE}", f"learn: target={target}")

        # Log.mdebug(f"{DNNManager._MODULE}", f"learn: before estimate.grad={estimate.grad}")
        # Log.mdebug(f"{DNNManager._MODULE}", f"learn: before target.grad={target.grad}")

        loss = self._loss(estimate, target)
        # Log.mdebug(f"{DNNManager._MODULE}", f"learn: loss={loss} typeof={type(loss)}")
        # loss = Utils.to_device(torch.Tensor([0.1]), Utils.hardware_check())

        self.optimizer.zero_grad()
        loss.backward()
        self.optimizer.step()

        # Log.mdebug(f"{DNNManager._MODULE}", f"learn: after estimate.grad={estimate.grad}")
        # Log.mdebug(f"{DNNManager._MODULE}", f"learn: after target.grad={target.grad}")

        # for p in self._online.parameters():
        #     print(p)

        return loss.item()

    def _loss(self, prev_arr, next_arr):
        return self.loss_fn(prev_arr, next_arr)

    def swap(self):
        # self._target.load_state_dict(self._online.state_dict())
        pass

    def save(self, path, filename):
        """Save the target net to file"""
        os.makedirs(path, exist_ok=True)
        torch.save(self._online, f"{path}/{filename}.pth")

    def load(self, path, filename):
        """Load the same model for both the networks"""
        # self._target.load_state_dict(torch.load(f"{path}/{filename}"))
        self._online.load_state_dict(torch.load(f"{path}/{filename}"))


class DNNFC1(nn.Module):
    def __init__(self, inputs_n, outputs_n,
                 last_activation_fn: DNNManager.LastActivationFn = DNNManager.LastActivationFn.NONE):
        super(DNNFC1, self).__init__()
        self.fc1 = nn.Linear(inputs_n, 256)
        # self.fc2 = nn.Linear(256, 512)
        # self.fc3 = nn.Linear(512, 1024)
        # self.fc4 = nn.Linear(1024, 512)
        # self.fc5 = nn.Linear(512, 256)
        self.fc6 = nn.Linear(256, outputs_n)

        self.relu_1 = nn.ReLU()
        self.relu_2 = nn.ReLU()
        self.relu_3 = nn.ReLU()
        self.relu_4 = nn.ReLU()
        self.relu_5 = nn.ReLU()
        self.softmax = nn.Softmax(dim=0)

        self._last_activation_fn = last_activation_fn

    # x represents our data
    def forward(self, input_matrix):
        x = torch.flatten(input_matrix, 0)
        x = self.fc1(x)
        x = self.relu_1(x)
        # x = self.fc2(x)
        # x = self.relu_2(x)
        # x = self.fc3(x)
        # x = self.relu_3(x)
        # x = self.fc4(x)
        # x = self.relu_4(x)
        # x = self.fc5(x)
        x = self.relu_5(x)
        x = self.fc6(x)
        output = self.softmax(x) if self._last_activation_fn == DNNManager.LastActivationFn.SOFTMAX else x
        return output


class ActorLoss(nn.Module):
    def __init__(self, weight=None, size_average=True):
        super(ActorLoss, self).__init__()

    def forward(self, inputs, targets):
        return torch.sum(torch.log(inputs) * targets)
