#  Copyright (c) 2023. Gabriele Proietti Mattia <pm.gabriele@gmail.com>
#  simulator-2023-cloudcom - Simpy simulator of energy schedulers
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.

from __future__ import annotations

import json
import os
import shutil
import sqlite3
import time

from torch.utils.tensorboard import SummaryWriter

import log
from job import Job
from log import Log
from reinforcement import ReinforcementAlgorithm
from service_discovery import ServiceDiscovery
from utils import Utils


# noinspection SqlNoDataSourceInspection
class ServiceDataStorage:
    _MODULE = "ServiceDataStorage"
    _PATH_PROJECT = os.path.dirname(__file__)

    # labels
    _TNSBOARD_MAX_EP_DATA = 5
    _TNSBOARD_CHART_RO_MIGRATION_RATIO = "RoMigration/EpLatest(nnn)MigrationRatio"
    _TNSBOARD_CHART_RO_REWARD = "RoLearning/EpLatest(nnn)Reward"
    _TNSBOARD_CHART_RO_DROP_RATE = "RoMetrics/EpLatest(nnn)DropRate"
    _TNSBOARD_CHART_RO_FORWARDED_RATE = "RoMetrics/EpLatest(nnn)ForwardedRate"
    _TNSBOARD_CHART_RO_REQUESTS_RATE = "RoMetrics/EpLatest(nnn)RequestsRate"
    _TNSBOARD_CHART_RO_REQUESTS_RATE_EFFECTIVE = "RoMetrics/EpLatest(nnn)RequestsRateEffective"
    _TNSBOARD_CHART_RO_LIFESPAN = "RoEnergy/EpLatest(nnn)Lifespan"
    _TNSBOARD_CHART_RO_BATTERY_RESIDUAL = "RoEnergy/EpLatest(nnn)BatteryResidual"
    _TNSBOARD_CHART_RO_BATTERY_DELTA = "RoEnergy/EpLatest(nnn)BatteryDelta"
    _TNSBOARD_CHART_RO_BATTERY_DISCHARGE = "RoEnergy/EpLatest(nnn)BatteryDischarge"
    _TNSBOARD_CHART_RO_OPERATIONS_TIME = "RoEnergy/EpLatest(nnn)OperationsTime"
    _TNSBOARD_CHART_RO_OPERATIONS_DISCHARGE = "RoEnergy/EpLatest(nnn)OperationsDischarge"

    _TNSBOARD_CHART_EP_LIFESPAN_MIN = "EpEnergy/LifespanMin"
    _TNSBOARD_CHART_EP_DROP_RATE = "EpPerformance/DropRate"
    _TNSBOARD_CHART_EP_FORWARDED_RATE = "EpPerformance/ForwardedRate"
    _TNSBOARD_CHART_EP_DROP_RATE_AVG = "EpPerformance/DropRateAvg"
    _TNSBOARD_CHART_EP_FORWARDED_RATE_AVG = "EpPerformance/ForwardedRateAvg"
    _TNSBOARD_CHART_EP_LOSS_ACTOR = "EpLearning/LossActor"
    _TNSBOARD_CHART_EP_LOSS_CRITIC = "EpLearning/LossCritic"

    _DB_METRIC_RO_LIFESPAN = "lifespan"
    _DB_METRIC_RO_BATTERY = "battery"
    _DB_METRIC_RO_REJECTED_PERC = "rejected_perc"
    _DB_METRIC_RO_FORWARDED_PERC = "forwarded_perc"
    _DB_METRIC_RO_POWER_CONSUMPTION = "power_consumption"

    def __init__(self, session_id: str, skip_db=True, skip_tensorboard=True):
        self._session_id = session_id
        self._skip_db = skip_db
        self._skip_tensorboard = skip_tensorboard

        self._session_folder = f"{ServiceDataStorage._PATH_PROJECT}/results/{self._session_id}"
        self._session_folder_weights = f"{ServiceDataStorage._PATH_PROJECT}/results/{self._session_id}/weights"
        self._session_folder_tensorboard = f"{ServiceDataStorage._PATH_PROJECT}/results/{self._session_id}/tensorboard"
        os.makedirs(self._session_folder, exist_ok=True)

        Log.minfo(ServiceDataStorage._MODULE, f"__init__: session_folder={self._session_folder}")

        # loggers
        if not skip_tensorboard:
            os.makedirs(self._session_folder_weights, exist_ok=True)
            os.makedirs(self._session_folder_tensorboard, exist_ok=True)
            Log.minfo(ServiceDataStorage._MODULE, f"__init__: session_folder_weights={self._session_folder_weights}")
            Log.minfo(ServiceDataStorage._MODULE,
                      f"__init__: session_folder_tnsboard={self._session_folder_tensorboard}")

            self._summary_writer = SummaryWriter(log_dir=self._session_folder_tensorboard)

        if not skip_db:
            self._db = sqlite3.connect(':memory:')  # f"{self._log_dir}/log.db")
            self._db_cur = self._db.cursor()

            self._init_db()

        self._counter_total_jobs = 0

    def _init_db(self):
        self._db_cur.execute('''CREATE TABLE episode (
                                                    episode integer, 
                                                    job_reject_perc real,
                                                    job_forwarded_perc real,
                                                    lifespan_min real,
                                                    loss_actor real,
                                                    loss_critic real
                                                )''')

        self._db_cur.execute('''CREATE TABLE round (
                                                    n integer,
                                                    episode integer, 
                                                    node_uid integer,
                                                    battery_residual real
                                                )''')

        self._db_cur.execute('''CREATE TABLE migration_ratio (
                                                    episode integer, 
                                                    round integer, 
                                                    node_uid_from integer, 
                                                    node_uid_to integer,
                                                    ratio real
                                                )''')

        self._db_cur.execute('''CREATE TABLE jobs (
                                                    uid integer, 
                                                    node_uid integer, 
                                                    episode integer,
                                                    round integer, 
                                                    eps real, 
                                                    state_snapshot text,
                                                    forwarded_to_node_uid integer,
                                                    forwarded_to_cloud integer,
                                                    executed integer, 
                                                    rejected integer, 
                                                    lost integer, 
                                                    done integer, 
                                                    reward real, 
                                                    time_total real, 
                                                    time_probing real, 
                                                    time_dispatching real, 
                                                    time_queue real,
                                                    time_execution real,
                                                    time_total_execution real,
                                                    generated_at real
                                                )''')

        self._db_cur.execute('''CREATE TABLE metrics_round (
                                                    node_uid integer,
                                                    round integer,
                                                    episode integer, 
                                                    metric text,
                                                    value real
                                                )''')

        self._db.commit()
        Log.minfo(ServiceDataStorage._MODULE, "DB init")

    def _copy_db_to_file(self):
        dbfile = f"{self._session_folder}/log.db"
        Log.minfo(ServiceDataStorage._MODULE, f"Copying memory db to file {dbfile}, please wait")
        start = time.time()

        new_db = sqlite3.connect(dbfile)
        query = "".join(line for line in self._db.iterdump())

        # Dump old database in the new one.
        new_db.executescript(query)
        new_db.close()

        Log.minfo(ServiceDataStorage._MODULE, f"Done in {time.time() - start:2f}")

    #
    # Stat data manager
    #

    def save_simulation_data(self, service_discovery: ServiceDiscovery):
        meta_filename = f"{self._session_folder}/meta.txt"
        discovery_dict = service_discovery.get_configuration_dict()
        meta_dict = {
            "session_id": self._session_id,
            "discovery": discovery_dict
        }

        with open(meta_filename, "w") as meta_file:
            print(json.dumps(meta_dict, indent="  "), file=meta_file)
            meta_file.close()

    def done_round_for_node(self, round_n, episode_n, node_uid, battery_residual):
        if self._skip_db:
            return

        self._db_cur.execute(
            f'''INSERT INTO round VALUES (
                        {round_n}, 
                        {episode_n}, 
                        {node_uid},
                        {battery_residual}
            )''')
        self._db.commit()

    def done_job(self, job: Job, reward: int):
        return
        """
        # noinspection SqlResolve
        self._db_cur.execute(f'''INSERT INTO jobs VALUES (
                                    "{job.get_uid()}", 
                                    {job.get_originator_node_uid()}, 
                                    {job.get_episode()}, 
                                    {job.get_eps()},
                                    "{job.get_state_snapshot_str()}",
                                    {job.get_forwarded_to_node_id()},
                                    {job.get_forwarded_to_cluster_id()},
                                    {1 if job.is_forwarded_to_cloud() else 0},
                                    {job.get_action(0)},
                                    {1 if job.is_executed() else 0}, 
                                    {1 if job.is_rejected() else 0},
                                    {1 if job.is_lost() else 0},
                                    {job.get_type()},
                                    {1 if job.is_over_deadline() else 0},
                                    {1 if job.is_done() else 0},
                                    {reward}, 
                                    {job.get_total_time()},
                                    {job.get_probing_time()},
                                    {job.get_dispatched_time()},
                                    {job.get_queue_time()},
                                    {job.get_time_execution()},
                                    {job.get_total_time_execution()},
                                    {job.get_generated_at()})
                                ''')
        self._db.commit()
        """
        # self._counter_total_jobs += 1

    def log_matrix_round(self, round_n, episode_n, migration_matrix, matrix_type="migration"):
        migration_dict = {}
        for i in range(len(migration_matrix)):
            for j in range(len(migration_matrix[i])):
                migration_dict[f"n_{i:02d}-{j:02d}"] = migration_matrix[i][j]

                if not self._skip_db:
                    self._db_cur.execute(
                        f'''INSERT INTO migration_ratio VALUES (
                                     {episode_n}, 
                                     {round_n}, 
                                     {i},
                                     {j}, 
                                     {migration_matrix[i][j]} 
                         )''')

        if matrix_type == "reward":
            tag_string = ServiceDataStorage._TNSBOARD_CHART_RO_REWARD.replace("(nnn)", f"{episode_n:05d}")
        else:
            tag_string = ServiceDataStorage._TNSBOARD_CHART_RO_MIGRATION_RATIO.replace("(nnn)", f"{episode_n:05d}")

        if not self._skip_tensorboard:
            self._summary_writer.add_scalars(tag_string, migration_dict, round_n)

    def log_metrics_all_nodes_round(self, episode_n, round_n,
                                    batteries_residual_perc=None,
                                    rejected_perc_dict=None,
                                    forwarded_perc_dict=None,
                                    arrival_rates_dict=None,
                                    lifespan_dict=None,
                                    battery_discharge_dict=None,
                                    requests_rate_effective=None,
                                    operations_time=None,
                                    operations_discharge=None,
                                    energy_consumption_dict=None
                                    ):

        if not self._skip_db:
            for node_uid in lifespan_dict.keys():
                self._log_db_metric(node_uid, episode_n, round_n,
                                    ServiceDataStorage._DB_METRIC_RO_LIFESPAN, lifespan_dict[node_uid])

            for node_uid in batteries_residual_perc.keys():
                self._log_db_metric(node_uid, episode_n, round_n,
                                    ServiceDataStorage._DB_METRIC_RO_BATTERY, batteries_residual_perc[node_uid])

            for node_uid in rejected_perc_dict.keys():
                self._log_db_metric(node_uid, episode_n, round_n,
                                    ServiceDataStorage._DB_METRIC_RO_REJECTED_PERC, rejected_perc_dict[node_uid])

            for node_uid in forwarded_perc_dict.keys():
                self._log_db_metric(node_uid, episode_n, round_n,
                                    ServiceDataStorage._DB_METRIC_RO_FORWARDED_PERC, forwarded_perc_dict[node_uid])

            for node_uid in energy_consumption_dict.keys():
                self._log_db_metric(node_uid, episode_n, round_n,
                                    ServiceDataStorage._DB_METRIC_RO_POWER_CONSUMPTION,
                                    energy_consumption_dict[node_uid])

            self._db.commit()

        if not self._skip_tensorboard:
            if rejected_perc_dict is not None:
                self._summary_writer.add_scalars(ServiceDataStorage._TNSBOARD_CHART_RO_DROP_RATE
                                                 .replace("(nnn)", f"{episode_n:05d}"), rejected_perc_dict, round_n)
            if forwarded_perc_dict is not None:
                self._summary_writer.add_scalars(ServiceDataStorage._TNSBOARD_CHART_RO_FORWARDED_RATE
                                                 .replace("(nnn)", f"{episode_n:05d}"), forwarded_perc_dict, round_n)
            if lifespan_dict is not None:
                self._summary_writer.add_scalars(ServiceDataStorage._TNSBOARD_CHART_RO_LIFESPAN
                                                 .replace("(nnn)", f"{episode_n:05d}"), lifespan_dict, round_n)
            if arrival_rates_dict is not None:
                self._summary_writer.add_scalars(ServiceDataStorage._TNSBOARD_CHART_RO_REQUESTS_RATE
                                                 .replace("(nnn)", f"{episode_n:05d}"), arrival_rates_dict, round_n)
            if requests_rate_effective is not None:
                self._summary_writer.add_scalars(ServiceDataStorage._TNSBOARD_CHART_RO_REQUESTS_RATE_EFFECTIVE
                                                 .replace("(nnn)", f"{episode_n:05d}"), requests_rate_effective,
                                                 round_n)

            # batteries
            if batteries_residual_perc is not None:
                self._summary_writer.add_scalars(ServiceDataStorage._TNSBOARD_CHART_RO_BATTERY_RESIDUAL
                                                 .replace("(nnn)", f"{episode_n:05d}"), batteries_residual_perc,
                                                 round_n)
            if battery_discharge_dict is not None:
                self._summary_writer.add_scalars(ServiceDataStorage._TNSBOARD_CHART_RO_BATTERY_DISCHARGE
                                                 .replace("(nnn)", f"{episode_n:05d}"), battery_discharge_dict, round_n)

            # operations
            if operations_time is not None:
                self._summary_writer.add_scalars(ServiceDataStorage._TNSBOARD_CHART_RO_OPERATIONS_TIME
                                                 .replace("(nnn)", f"{episode_n:05d}"), operations_time, round_n)
            if operations_discharge is not None:
                self._summary_writer.add_scalars(ServiceDataStorage._TNSBOARD_CHART_RO_OPERATIONS_DISCHARGE
                                                 .replace("(nnn)", f"{episode_n:05d}"), operations_discharge, round_n)

            min_battery = min(batteries_residual_perc.values())
            batteries_delta = {}
            for node_uid in batteries_residual_perc.keys():
                batteries_delta[node_uid] = batteries_residual_perc[node_uid] - min_battery

            self._summary_writer.add_scalars(ServiceDataStorage._TNSBOARD_CHART_RO_BATTERY_DELTA
                                             .replace("(nnn)", f"{episode_n:05d}"), batteries_delta, round_n)

    def log_metrics_episode(self, episode_n, rejected_perc_dict, forwarded_perc_dict, lifespan_min=.0, loss_actor=.0,
                            loss_critic=.0):
        avg_drop_rate = Utils.avg(list(rejected_perc_dict.values()))
        avg_forwarded_rate = Utils.avg(list(forwarded_perc_dict.values()))

        if not self._skip_tensorboard:
            self._remove_useless_charts(episode_n)

            self._summary_writer.add_scalar("EpEnergy/LifespanMin", lifespan_min, episode_n)
            self._summary_writer.add_scalars("EpPerformance/DropRate", rejected_perc_dict, episode_n)
            self._summary_writer.add_scalars("EpPerformance/ForwardedRate", forwarded_perc_dict, episode_n)

            self._summary_writer.add_scalar("EpPerformance/DropRateAvg", avg_drop_rate, episode_n)
            self._summary_writer.add_scalar("EpPerformance/ForwardedRateAvg", avg_forwarded_rate, episode_n)

            self._summary_writer.add_scalar("EpLearning/LossActor", loss_actor, episode_n)
            self._summary_writer.add_scalar("EpLearning/LossCritic", loss_critic, episode_n)

        # save to db
        if not self._skip_db:
            self._db_cur.execute(
                f'''INSERT INTO episode VALUES (
                            {episode_n}, 
                            {avg_drop_rate},
                            {avg_forwarded_rate}, 
                            {lifespan_min}, 
                            {loss_actor},
                            {loss_critic}
                )''')
            self._db.commit()

        Log.minfo(f"{ServiceDataStorage._MODULE}",
                  f"ended episode={episode_n}, drop_rate={avg_drop_rate} lifespan_min={lifespan_min} loss={loss_actor} loss_critic={loss_critic}")

    def save_models(self, filename, rl_model: ReinforcementAlgorithm = None):
        rl_model.save_models(self._session_folder_weights, filename)

    def _remove_useless_charts(self, episode_n):
        if episode_n <= ServiceDataStorage._TNSBOARD_MAX_EP_DATA:
            return

        # base logs to keep
        to_keep_list = [
            ServiceDataStorage._TNSBOARD_CHART_EP_LIFESPAN_MIN.replace("/", "_"),
            ServiceDataStorage._TNSBOARD_CHART_EP_DROP_RATE.replace("/", "_"),
            ServiceDataStorage._TNSBOARD_CHART_EP_FORWARDED_RATE.replace("/", "_"),
            ServiceDataStorage._TNSBOARD_CHART_EP_DROP_RATE_AVG.replace("/", "_"),
            ServiceDataStorage._TNSBOARD_CHART_EP_FORWARDED_RATE_AVG.replace("/", "_"),
            ServiceDataStorage._TNSBOARD_CHART_EP_LOSS_ACTOR.replace("/", "_"),
            ServiceDataStorage._TNSBOARD_CHART_EP_LOSS_CRITIC.replace("/", "_")
        ]

        # keep only latest max_ep_data items
        for i in range(episode_n - ServiceDataStorage._TNSBOARD_MAX_EP_DATA, episode_n + 1):
            log.Log.mdebug(ServiceDataStorage._MODULE,
                           f"_remove_useless_charts: we need to keep data for episode {i:05d} is to keep, episode_n={episode_n}")

            to_keep_list.append(
                ServiceDataStorage._TNSBOARD_CHART_RO_MIGRATION_RATIO.replace("(nnn)", f"{i:05d}").replace("/", "_"))
            to_keep_list.append(
                ServiceDataStorage._TNSBOARD_CHART_RO_REWARD.replace("(nnn)", f"{i:05d}").replace("/", "_"))
            to_keep_list.append(
                ServiceDataStorage._TNSBOARD_CHART_RO_DROP_RATE.replace("(nnn)", f"{i:05d}").replace("/", "_"))
            to_keep_list.append(
                ServiceDataStorage._TNSBOARD_CHART_RO_FORWARDED_RATE.replace("(nnn)", f"{i:05d}").replace("/", "_"))
            to_keep_list.append(
                ServiceDataStorage._TNSBOARD_CHART_RO_REQUESTS_RATE_EFFECTIVE.replace("(nnn)", f"{i:05d}").replace("/",
                                                                                                                   "_"))
            to_keep_list.append(
                ServiceDataStorage._TNSBOARD_CHART_RO_LIFESPAN.replace("(nnn)", f"{i:05d}").replace("/", "_"))
            to_keep_list.append(
                ServiceDataStorage._TNSBOARD_CHART_RO_BATTERY_RESIDUAL.replace("(nnn)", f"{i:05d}").replace("/", "_"))
            to_keep_list.append(
                ServiceDataStorage._TNSBOARD_CHART_RO_BATTERY_DELTA.replace("(nnn)", f"{i:05d}").replace("/", "_"))
            to_keep_list.append(
                ServiceDataStorage._TNSBOARD_CHART_RO_BATTERY_DISCHARGE.replace("(nnn)", f"{i:05d}").replace("/", "_"))

        # for file_to_keep_name in to_keep_list:
        #     log.Log.mdebug(ServiceDataStorage._MODULE, f"_remove_useless_charts: KEEPING {file_to_keep_name}")

        for filename in os.listdir(self._session_folder_tensorboard):
            filename_full = os.path.join(self._session_folder_tensorboard, filename)

            if os.path.isdir(filename_full):
                # log.Log.mdebug(ServiceDataStorage._MODULE, f"_remove_useless_charts: {filename} is currently analyzed")

                # keep only last ServiceDataStorage._TNSBOARD_MAX_EP_DATA episodes
                to_keep = False

                for filename_to_keep in to_keep_list:
                    if filename_to_keep in filename:
                        # log.Log.mdebug(ServiceDataStorage._MODULE, f"_remove_useless_charts: {filename_full} is to keep")
                        to_keep = True
                        break

                if not to_keep:
                    log.Log.mdebug(ServiceDataStorage._MODULE,
                                   f"_remove_useless_charts: {filename_full} has been removed")
                    shutil.rmtree(filename_full)

    def _log_db_metric(self, node_uid, episode, round_n, metric_tag, metric_value):
        self._db_cur.execute(
            f'''INSERT INTO metrics_round VALUES (
                                    {node_uid}, 
                                    {round_n}, 
                                    {episode}, 
                                    "{metric_tag}", 
                                    {metric_value} 
                        )''')

        self._db.commit()

    def done_experiment(self):
        if not self._skip_db:
            self._copy_db_to_file()
            self._db.close()

        # self._save_models()

    def get_session_folder(self):
        return self._session_folder