#  Copyright (c) 2023. Gabriele Proietti Mattia <pm.gabriele@gmail.com>
#  simulator-2023-cloudcom - Simpy simulator of energy schedulers
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.

from datetime import datetime
from typing import List

import numpy as np
import torch


class Utils:

    @staticmethod
    def to_tensor(obj, requires_grad=False):
        t = torch.Tensor(obj)
        t.requires_grad = requires_grad
        return t

    @staticmethod
    def softmax(x):
        """Compute softmax values for each sets of scores in x."""
        e_x = np.exp(x - np.max(x))
        return e_x / e_x.sum(axis=0)  # only difference

    @staticmethod
    def current_time_string():
        return datetime.now().strftime("%Y%m%d-%H%M%S")

    @staticmethod
    def avg(arr: list):
        return sum(arr) / len(arr)

    @staticmethod
    def hardware_check():
        device = "cuda:0" if torch.cuda.is_available() else "cpu"
        print("Actual device: ", device)
        if 'cuda' in device:
            print("Device info: {}".format(str(torch.cuda.get_device_properties(device)).split("(")[1])[:-1])

        return device

    @staticmethod
    def to_device(data, device):
        """Move tensor(s) to chosen device"""
        if isinstance(data, (list, tuple)):
            return [Utils.to_device(x, device) for x in data]
        return data.to(device, non_blocking=True)

    @staticmethod
    def print_matrix(matrix: List[List[float]]):
        for i in range(len(matrix)):
            for j in range(len(matrix[i])):
                print(f"{matrix[i][j]:.4f}", end=("\t" if j != len(matrix[i]) - 1 else ""))
            print()

    @staticmethod
    def line_by_two_points(x1, x2, y1, y2):
        m = (y1 - y2) / (x1 - x2)
        q = (x1 * y2 - x2 * y1) / (x1 - x2)
        return m, q

    @staticmethod
    def convert_policy_to_migration_ratios(n_nodes: int, policy_t: torch.Tensor):
        policy_l = policy_t.tolist()

        migration_matrix = []
        for i in range(n_nodes):
            migration_matrix.append([])
            for j in range(n_nodes):
                migration_matrix[i].append(policy_l[i * n_nodes + j])

        # Log.mdebug(Orchestrator._MODULE, f"_convert_policy_to_migration_ratios: {migration_matrix}")

        m_stochastic = []
        # create stochastic vectors
        for i in range(n_nodes):
            row_softmax = Utils.softmax(np.array(migration_matrix[i]))
            m_stochastic.append(row_softmax.tolist())

        return m_stochastic
