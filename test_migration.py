#  Copyright (c) 2023. Gabriele Proietti Mattia <pm.gabriele@gmail.com>
#  simulator-2023-cloudcom - Simpy simulator of energy schedulers
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.

import random

from node import Node


def act():
    migration_ratios = [0.2, 0.1, 0.3, 0.2]
    rnd = random.random()

    cumulative_probability = 0.2  # self._migration_ratio_local
    if rnd < cumulative_probability:
        return Node.SchedulingAction.EXECUTE_LOCALLY, -1

    for i, ratio in enumerate(migration_ratios):
        cumulative_probability += ratio
        if rnd < cumulative_probability:
            action = Node.SchedulingAction.FORWARD_TO_NEIGHBOUR
            return action, i + 1


for i in range(100):
    print(act())
