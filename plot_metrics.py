#  Copyright (c) 2023. Gabriele Proietti Mattia <pm.gabriele@gmail.com>
#  simulator-2023-cloudcom - Simpy simulator of energy schedulers
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
import sqlite3
from enum import Enum

from log import Log
from plot import PlotUtils, Plot

EXPERIMENT_ID = "20230720-173604"
EXPERIMENT_SAVED_FOLDER = f"saved/{EXPERIMENT_ID}"

PLOT_DIR = f"{EXPERIMENT_SAVED_FOLDER}/plot"
os.makedirs(PLOT_DIR, exist_ok=True)


# noinspection SqlNoDataSourceInspection
class PlotExperiment:
    _PATH_PROJECT = os.path.dirname(__file__)

    class Metric(Enum):
        LIFESPAN = 0
        BATTERY = 1
        REJECTED_PERC = 2
        FORWARDED_PERC = 3
        POWER_CONSUMPTION = 4

    dict_y_label = {
        Metric.BATTERY: "Battery Percentage (\%)",
        Metric.LIFESPAN: "Lifespan (s)",
        Metric.REJECTED_PERC: "Rejected (\%)",
        Metric.FORWARDED_PERC: "Forwarded (\%)",
        Metric.POWER_CONSUMPTION: "W",
    }

    dict_filename = {
        Metric.BATTERY: "battery",
        Metric.LIFESPAN: "lifespan",
        Metric.REJECTED_PERC: "rejected_perc",
        Metric.FORWARDED_PERC: "forwarded_perc",
        Metric.POWER_CONSUMPTION: "power_consumption",
    }

    _QUERY_NODES_IDS = lambda: '''
        SELECT DISTINCT node_uid 
        FROM metrics_round
    '''

    _QUERY_BATTERY = lambda node_uid: f'''
        SELECT * 
        FROM metrics_round
        WHERE 
            metric = "battery"
            AND
            node_uid = {node_uid}
    '''

    _QUERY_LIFESPAN = lambda node_uid: f'''
        SELECT * 
        FROM metrics_round
        WHERE 
            metric = "lifespan"
            AND
            node_uid = {node_uid}
    '''

    _QUERY_METRICS_GENERIC = lambda node_uid, metric_name: f'''
        SELECT * 
        FROM metrics_round
        WHERE 
            metric = "{metric_name}"
            AND
            node_uid = {node_uid}
    '''

    _QUERY_MIGRATION_RATIO = lambda node_uid_from, node_uid_to: f'''
        SELECT * 
        FROM migration_ratio
        WHERE 
            node_uid_from = {node_uid_from}
            AND
            node_uid_to = {node_uid_to}
    '''

    def __init__(self, exp_folder):
        self._exp_folder = exp_folder

        self._db = sqlite3.connect(f"{exp_folder}/log.db")
        self._db_cur = self._db.cursor()

        self._nodes_ids = []

    def _get_nodes_id(self):
        res = self._db_cur.execute(PlotExperiment._QUERY_NODES_IDS())
        for line in res:
            node_id = int(line[0])
            self._nodes_ids.append(node_id)

        sorted(self._nodes_ids)
        return self._nodes_ids

    def _plot_metric(self, metric: Metric, average_every=100, multiplier_factor=1.0):
        Log.minfo(self._module(), f"Plotting {metric.name}...")

        legend = []
        values_x_all = []
        values_y_all = []

        for node_id in self._nodes_ids:
            values_x = []
            values_y = []
            avg_values = []
            avg_values_len = 0
            value_i = 0

            res = []
            if metric == PlotExperiment.Metric.LIFESPAN:
                res = self._db_cur.execute(PlotExperiment._QUERY_LIFESPAN(node_id))
            elif metric == PlotExperiment.Metric.BATTERY:
                res = self._db_cur.execute(PlotExperiment._QUERY_BATTERY(node_id))
            elif metric == PlotExperiment.Metric.REJECTED_PERC:
                res = self._db_cur.execute(PlotExperiment._QUERY_METRICS_GENERIC(node_id, "rejected_perc"))
            elif metric == PlotExperiment.Metric.FORWARDED_PERC:
                res = self._db_cur.execute(PlotExperiment._QUERY_METRICS_GENERIC(node_id, "forwarded_perc"))
            elif metric == PlotExperiment.Metric.POWER_CONSUMPTION:
                res = self._db_cur.execute(PlotExperiment._QUERY_METRICS_GENERIC(node_id, "power_consumption"))
            else:
                Log.mfatal(self._module(), f"Metric \"{metric}\" not valid!")

            for line in res:
                round_n = int(line[1])
                value = float(line[4]) * multiplier_factor
                # Log.mdebug(self._module(), f"_plot_metric: value={value}")

                if value_i == 0:
                    values_x.append(round_n)
                    values_y.append(value)
                    value_i += 1
                    continue

                avg_values.append(value)
                avg_values_len += 1
                if avg_values_len == average_every:
                    # Log.mdebug(self._module(), f"_plot_metric: avg_values_len={avg_values_len}")

                    values_x.append(round_n)
                    values_y.append(sum(avg_values) / avg_values_len)
                    avg_values = []
                    avg_values_len = 0

            values_x_all.append(values_x)
            values_y_all.append(values_y)
            legend.append(f"Node {node_id}")

        Plot.multi_plot(values_x_all, values_y_all, "Round",
                        PlotExperiment.dict_y_label[metric], use_markers=False, linewidth=1.0,
                        legend=legend, fullpath=f"{PLOT_DIR}/{PlotExperiment.dict_filename[metric]}.pdf")

        return values_x_all, values_y_all, legend

    def _plot_migration_ratios(self, average_every=100, multiplier_factor=1.0):
        legend = []

        possible_ratios = []
        for node_id in self._nodes_ids:
            for node_id_2 in self._nodes_ids:
                if node_id == node_id_2:
                    continue

                possible_ratios.append((node_id, node_id_2))

        ratios_x_all = []
        ratios_y_all = []

        # retrieve and plot
        for node_tuple in possible_ratios:
            ratios_x = []
            ratios_y = []

            avg_values = []
            avg_values_len = 0
            value_i = 0

            legend.append(f"$m_{{ {node_tuple[0]}{node_tuple[1]} }}$")

            res = self._db_cur.execute(PlotExperiment._QUERY_MIGRATION_RATIO(node_tuple[0], node_tuple[1]))
            for line in res:
                round_n = int(line[1])
                ratio = float(line[4]) * multiplier_factor

                if value_i == 0:
                    ratios_x.append(round_n)
                    ratios_y.append(ratio)
                    value_i += 1
                    continue

                avg_values.append(ratio)
                avg_values_len += 1
                if avg_values_len == average_every:
                    # Log.mdebug(self._module(), f"_plot_metric: avg_values_len={avg_values_len}")

                    ratios_x.append(round_n)
                    ratios_y.append(sum(avg_values) / avg_values_len)
                    avg_values = []
                    avg_values_len = 0

                # ratios_x.append(round_n)
                # ratios_y.append(ratio)

            ratios_x_all.append(ratios_x)
            ratios_y_all.append(ratios_y)

        Plot.multi_plot(ratios_x_all, ratios_y_all, "Round", "Migration Ratio", linewidth=1.0,
                        use_markers=False, legend=legend, fullpath=f"{PLOT_DIR}/migration_ratios.pdf")

        return ratios_x_all, ratios_y_all, legend

    def _plot_metrics_3(self, plots_x_arrays, plots_y_arrays, legends, x_labels, y_labels, y_lims=None):
        Log.minfo(self._module(), f"Plotting metrics_3...")
        Plot.subplot_vertical(plots_x_arrays, plots_y_arrays, legends, x_labels, y_labels,
                              fullpath=f"{PLOT_DIR}/subplot_3x1.pdf",
                              max_h=4.8, max_w=5.0, y_lims=y_lims, height_ratios=[1.5, 1, 1])

    def _plot_metrics_4(self, plots_x_arrays, plots_y_arrays, legends, x_labels, y_labels, y_lims=None):
        Log.minfo(self._module(), f"Plotting metrics_4...")
        Plot.subplot_vertical(plots_x_arrays, plots_y_arrays, legends, x_labels, y_labels,
                              fullpath=f"{PLOT_DIR}/subplot_4x1.pdf",
                              max_h=5.2, max_w=5.0, y_lims=y_lims, height_ratios=[1.5, 1.2, 1, 1])

    def run(self):
        Log.minfo(self._module(), f"Getting nodes ids...{self._get_nodes_id()}")
        average_every = 100
        bat_x, bat_y, bat_l = self._plot_metric(PlotExperiment.Metric.BATTERY, average_every=1, multiplier_factor=100.0)
        lif_x, lif_y, lif_l = self._plot_metric(PlotExperiment.Metric.LIFESPAN, average_every=10)
        rej_x, rej_y, rej_l = self._plot_metric(PlotExperiment.Metric.REJECTED_PERC, average_every=average_every, multiplier_factor=100.0)
        for_x, for_y, for_l = self._plot_metric(PlotExperiment.Metric.FORWARDED_PERC, average_every=average_every, multiplier_factor=100.0)
        pow_x, pow_y, pow_l = self._plot_metric(PlotExperiment.Metric.POWER_CONSUMPTION, average_every=average_every)

        Log.minfo(self._module(), "Plotting migration ratios...")
        mig_x, mig_y, mig_l = self._plot_migration_ratios(average_every=200, multiplier_factor=100.0)

        self._plot_metrics_3([
            bat_x,
            pow_x,
            rej_x,
        ], [
            bat_y,
            pow_y,
            rej_y,
        ], [
            [rf"Node {i}" for i in range(len(bat_l))],
            None,  # [rf"$\lambda = {7 + i}$" for i in range(len(pow_l))],
            None,  # [rf"$\lambda = {7 + i}$" for i in range(len(rej_l))],
        ], [
            "Round",
            "Round",
            "Round",
        ], [
            r"Battery (\%)",
            r"Power (W)",
            r"Drop Rate (\%)",
        ], [
            None,
            None,
            [0, 10.0]
        ])

        self._plot_metrics_4([
            bat_x,
            mig_x,
            pow_x,
            rej_x,
        ], [
            bat_y,
            mig_y,
            pow_y,
            rej_y,
        ], [
            [rf"Node {i}" for i in range(len(bat_l))],
            [leg if max(mig_y[i]) > 0.02 else f"_{leg}" for i, leg in enumerate(mig_l)],
            None,  # [rf"$\lambda_{i} = {7 + i}$" for i in range(len(pow_l))],
            None,  # [rf"$\lambda_{i} = {7 + i}$" for i in range(len(rej_l))],
        ], [
            "Round",
            "Round",
            "Round",
            "Round",
        ], [
            r"Battery (\%)",
            r"Migr. Ratio (\%)",
            r"Power (W)",
            r"Drop Rate (\%)",
        ], [
            None,
            None,
            None,
            [0, 10.0],
        ])

        Log.minfo(self._module(), "Completed plotting")

    def close(self):
        self._db_cur.close()
        self._db.close()

    def _module(self):
        return "PlotMetrics"


def main():
    PlotUtils.use_tex()
    e = PlotExperiment(exp_folder=EXPERIMENT_SAVED_FOLDER)
    e.run()

    e.close()


if __name__ == "__main__":
    main()
