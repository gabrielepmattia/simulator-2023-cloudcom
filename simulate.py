#  Copyright (c) 2023. Gabriele Proietti Mattia <pm.gabriele@gmail.com>
#  simulator-2023-cloudcom - Simpy simulator of energy schedulers
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.

from __future__ import annotations

import time
from typing import List

import simpy

from best import Best
from log import Log
from models import NodeSpec, SolarTraceSpec
from node import Node
from reinforcement import ReinforcementAlgorithm
from service_data_storage import ServiceDataStorage
from service_discovery import ServiceDiscovery
from utils import Utils

"""
Run the simulation of deadline scheduling
"""


class Simulate:
    _MODULE = "Simulate"

    def __init__(self, service_data_storage: ServiceDataStorage, simulation_time=1e10, episode_n=0, best: Best = None,
                 round_time_s=30, stop_when_node_shutdown=False,
                 # nodes and scheduler
                 scheduler_type=Node.SchedulerType.FIXED_POLICY,
                 scheduler_fixed_policy_algorithm=Node.SchedulerFixedPolicyAlgorithm.RATIO_ADAPTIVE_STEP,
                 nodes_spec: List[NodeSpec] = (),
                 # rl
                 reinforcement_algorithm_array: List[ReinforcementAlgorithm] = (),
                 # solar solar_traces
                 solar_traces_enabled=False,
                 solar_traces_csv_list=(),
                 solar_traces_spec=(),
                 solar_traces_csv_col=1,
                 solar_traces_to_watt_multiplier=1000
                 ):

        self._env = simpy.Environment()

        self._nodes_spec = nodes_spec  # type: List[NodeSpec]
        """Nodes specification parameters and topology"""
        self._scheduler_type = scheduler_type
        """Scheduler type used by all nodes"""
        self._scheduler_fixed_policy_algorithm = scheduler_fixed_policy_algorithm
        """Scheduler algorithm used by all nodes"""
        self._stop_when_node_shutdown = stop_when_node_shutdown
        """Stop simulation when the node shutdowns"""

        self._episode_n = episode_n
        """Current episode number"""
        self._simulation_time = simulation_time
        """Maximum simulation time per episode"""
        self._service_data_storage = service_data_storage
        """Data storage service"""
        self._service_discovery = None
        """Discovery service to be created"""
        self._best = best
        """Object with best values"""
        self._round_time_s = round_time_s
        """Round time"""

        # rl
        self._rl_algorithm_array = reinforcement_algorithm_array
        """Array of RL algorithm, one per node"""
        self._device = Utils.hardware_check()
        """GPU device for DNNs"""

        # solar
        self._solar_traces_enabled = solar_traces_enabled
        """If solar solar_traces should be used"""
        self._solar_traces_csv_list = solar_traces_csv_list
        """List of csv with solar solar_traces"""
        self._solar_traces_spec = solar_traces_spec  # type: List[SolarTraceSpec]
        """Mapping of solar solar_traces to nodes"""
        self._solar_traces_csv_col = solar_traces_csv_col
        """Column of the solar solar_traces csv file where solar power is described"""
        self._solar_traces_to_watt_multiplier = solar_traces_to_watt_multiplier

        # self._solar_traces_by_id = {}
        self._solar_spec_by_node_id = {}
        for solar_spec in solar_traces_spec:
            self._solar_spec_by_node_id[solar_spec.node_id] = solar_spec

        self._init = False
        """If simulation has been init"""

        self._time_sim_start = 0.0

        self._log_sim_params()

    def init(self):
        """Init nodes and params"""
        # create nodes
        nodes = [
            #     self._create_node(self._env, node_id=0, rate_l=3.0, rate_mu=15.0),
            #     self._create_node(self._env, node_id=1, rate_l=7.0, rate_mu=15.0),
            #     self._create_node(self._env, node_id=2, rate_l=15.0, rate_mu=15.0)
        ]

        # nodes.append(self._create_node(self._env, node_id=3, rate_l=10, rate_mu=15))
        # nodes.append(self._create_node(self._env, node_id=4, rate_l=15, rate_mu=15))

        topology = {
            #    0: [1, 2],  # , 3, 4],
            #    1: [0, 2],  # , 3, 4],
            #    2: [0, 1],  # , 3, 4],
            #    3: [0, 1, 2, 4],
            #    4: [0, 1, 2, 3],
        }

        for node_spec in self._nodes_spec:
            node_solar_spec = self._solar_spec_by_node_id[node_spec.id]  # type: SolarTraceSpec
            nodes.append(Node(self._env,
                              node_spec.id,
                              simulation_time=self._simulation_time,
                              round_time_s=self._round_time_s,
                              stop_when_node_shutdown=self._stop_when_node_shutdown,
                              # scheduler
                              scheduler_type=self._scheduler_type,
                              scheduler_fixed_policy_algorithm=self._scheduler_fixed_policy_algorithm,
                              # rates
                              rate_l=node_spec.l,
                              rate_mu=node_spec.m,
                              # solar
                              solar_trace_enabled=self._solar_traces_enabled,
                              solar_trace_csv=self._solar_traces_csv_list[node_solar_spec.trace_id],
                              solar_trace_csv_col=self._solar_traces_csv_col,
                              solar_trace_spec=node_solar_spec,
                              solar_trace_to_watt_multiplier=self._solar_traces_to_watt_multiplier,
                              # net
                              net_speed_client_node_mbits=200,
                              net_speed_node_node_mbits=300,
                              # node info
                              max_jobs_in_queue=5,
                              job_payload_size_mb=0.1,
                              distribution_arrivals=Node.DistributionArrivals.POISSON,
                              # energy
                              battery_total_capacity_wh=node_spec.battery_capacity_wh,
                              battery_initial_capacity_wh=node_spec.battery_fill * node_spec.battery_capacity_wh,
                              energy_cpu_w=2.7,  # 2.97,
                              energy_transmission_w=0.3,  # 0.015,
                              energy_idle_w=2.0,
                              # distributions
                              # distribution_network_probing_sigma=0.0001,
                              distribution_network_forwarding_sigma=0.00002,
                              # learning
                              episode_n=self._episode_n,
                              reinforcement_learning_algorithm=self._rl_algorithm_array[node_spec.id],
                              )
                         )

            topology[node_spec.id] = node_spec.neighbors

        # add them discovery service
        self._service_discovery = ServiceDiscovery(nodes, topology)

        # init nodes services, and data
        for node in nodes:
            node.set_service_discovery(self._service_discovery)
            node.set_service_data_storage(self._service_data_storage)

        # save meta
        self._service_data_storage.save_simulation_data(self._service_discovery)

        self._init = True

        return len(nodes)

    def _log_process(self, env: simpy.Environment):
        while True:
            yield env.timeout(100)

            speed_up = float(self._env.now) / (time.time() - self._time_sim_start)
            remaining_time = (self._simulation_time - self._env.now) / speed_up

            Log.minfo(self._MODULE, f"_log_process: "
                                    f"t={self._env.now}/{self._simulation_time} "
                                    f"speedup=x{speed_up:.1f} "
                                    f"remaining_mins={remaining_time / 60:.1f}")

    def _simulate(self):
        if not self._init:
            Log.mfatal(Simulate._MODULE, "_simulate: please call init() before starting the simulation")

        self._time_sim_start = time.time()
        self._env.process(self._log_process(self._env))

        Log.minfo(Simulate._MODULE, f"Started simulation: episode_n={self._episode_n}")
        self._env.run(until=self._simulation_time)
        Log.minfo(Simulate._MODULE, f"Simulation ended: episode_n={self._episode_n}")

    def _log_sim_params(self):
        Log.minfo(Simulate._MODULE, f"params: simulation_time={self._simulation_time}")

    def simulate(self):
        self._simulate()

    def get_discovery_service(self) -> ServiceDiscovery:
        return self._service_discovery
